package zw.co.escrow.brokerussd.pojos.register;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class Custodian {

    @JsonProperty("Name")
    private String name;
    @JsonProperty("Code")
    private String code;

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Code")
    public String getCode() {
        return code;
    }

    @JsonProperty("Code")
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Custodian{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
