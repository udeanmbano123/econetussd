/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tavman
 */
@Entity
@Table(name = "ussd_mobile")
@XmlRootElement
public class UssdMobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "CDS_Number")
    private String cDSNumber;
    @Column(name = "pin")
    private String pin;
    @Column(name = "mobile")
    private String mobile;

    @Column(name = "transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @Column(name = "session_complete")
    private boolean sessionComplete;

    public UssdMobile() {
    }

    public UssdMobile(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCDSNumber() {
        return cDSNumber;
    }

    public void setCDSNumber(String cDSNumber) {
        this.cDSNumber = cDSNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public boolean isSessionComplete() {
        return sessionComplete;
    }

    public void setSessionComplete(boolean sessionComplete) {
        this.sessionComplete = sessionComplete;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UssdMobile)) {
            return false;
        }
        UssdMobile other = (UssdMobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UssdMobile{" +
                "id=" + id +
                ", cDSNumber='" + cDSNumber + '\'' +
                ", pin='" + pin + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
