package zw.co.escrow.brokerussd.pojos.register;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class MyOrders {

    @JsonProperty
    private String id;

    @JsonProperty
    private String desc;

    @JsonProperty
    private String price;

    @JsonProperty
    private String status;

    @JsonProperty
    private String counter;

    @JsonProperty
    private String volume;

    @JsonProperty
    private String date;

    @JsonProperty
    private String type;

    @JsonProperty
    private String fullname;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String desc)
    {
        this.desc = desc;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getCounter ()
    {
        return counter;
    }

    public void setCounter (String counter)
    {
        this.counter = counter;
    }

    public String getVolume ()
    {
        return volume;
    }

    public void setVolume (String volume)
    {
        this.volume = volume;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getFullname ()
    {
        return fullname;
    }

    public void setFullname (String fullname)
    {
        this.fullname = fullname;
    }

    @Override
    public String toString()
    {
        return "MyOrders [id = "+id+", " +
                "desc = "+desc+", " +
                "price = "+price+", " +
                "status = "+status+"," +
                " counter = "+counter+"," +
                " volume = "+volume+", " +
                "date = "+date+"," +
                " type = "+type+", " +
                "fullname = "+fullname+"]";
    }
}
