/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tavman
 */
@Entity
@Table(name = "register_session")
public class RegisterSession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "cds_number")
    private String cdsNumber;

    @Column(name = "surname")
    private String surname;

    @Column(name = "title")
    private String title;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "national_id")
    private String nationalId;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "dob")
    private String dob;

    @Column(name = "phone")
    private String phone;

    @Column(name = "transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    private String gender;

    @Column(name = "custodian")
    private String custodian;

    @Column(name = "bank")
    private String bank;

    @Column(name = "branch")
    private String branch;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "personal_details")
    private Boolean personalDetails;

    @Column(name = "name_details")
    private Boolean nameDetails;

    @Column(name = "addresses")
    private Boolean addresses;

    @Column(name = "bank_details")
    private Boolean bankDetails;

    @Column(name = "account")
    private Boolean account;

    @Column(name = "custodian_menu")
    private Boolean custodianMenu;

    @Column(name = "on_banks")
    private Boolean onBanks;

    @Column(name = "current_page")
    private Integer currentPage;

    @Column(name = "mno")
    private Boolean mno;

    @Column(name = "pin")
    private String pin;

    @Column(name = "ecnumber")
    private String ecnumber;

    @Column(name = "mobilenumber")
    private String mobilenumber;

    public RegisterSession() {
    }

    public RegisterSession(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCdsNumber() {
        return cdsNumber;
    }

    public void setCdsNumber(String cdsNumber) {
        this.cdsNumber = cdsNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustodian() {
        return custodian;
    }

    public void setCustodian(String custodian) {
        this.custodian = custodian;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Boolean getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(Boolean personalDetails) {
        this.personalDetails = personalDetails;
    }

    public Boolean getNameDetails() {
        return nameDetails;
    }

    public void setNameDetails(Boolean nameDetails) {
        this.nameDetails = nameDetails;
    }

    public Boolean getAddresses() {
        return addresses;
    }

    public void setAddresses(Boolean addresses) {
        this.addresses = addresses;
    }

    public Boolean getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(Boolean bankDetails) {
        this.bankDetails = bankDetails;
    }

    public Boolean getAccount() {
        return account;
    }

    public void setAccount(Boolean account) {
        this.account = account;
    }

    public Boolean getCustodianMenu() {
        return custodianMenu;
    }

    public void setCustodianMenu(Boolean custodianMenu) {
        this.custodianMenu = custodianMenu;
    }

    public Boolean getOnBanks() {
        return onBanks;
    }

    public void setOnBanks(Boolean onBanks) {
        this.onBanks = onBanks;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Boolean getMno() {
        return mno;
    }

    public void setMno(Boolean mno) {
        this.mno = mno;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public String getEcNumber() {
        return ecnumber;
    }

    public void setEcnumber(String ecnumber) {
        this.ecnumber = ecnumber;
    }
    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobileNumber(String ecnumber) {
        this.mobilenumber = mobilenumber;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegisterSession)) {
            return false;
        }
        RegisterSession other = (RegisterSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RegisterSession{" +
                "id=" + id +
                ", cdsNumber='" + cdsNumber + '\'' +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", nationalId='" + nationalId + '\'' +
                ", nationality='" + nationality + '\'' +
                ", dob='" + dob + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", gender=" + gender +
                ", custodian='" + custodian + '\'' +
                ", bank='" + bank + '\'' +
                ", branch='" + branch + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
