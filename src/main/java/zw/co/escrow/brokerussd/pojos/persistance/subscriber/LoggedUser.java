package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class LoggedUser {

    @JsonProperty
    private String Active;

    @JsonProperty
    private String Email;

    @JsonProperty
    private String CdsNumber;

    public String getActive() {
        return Active;
    }

    public void setActive(String Active) {
        this.Active = Active;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getCdsNumber() {
        return CdsNumber;
    }

    public void setCdsNumber(String CdsNumber) {
        this.CdsNumber = CdsNumber;
    }

    @Override
    public String toString() {
        return "LoggedUser [Active = " + Active + ", Email = " + Email + ", CdsNumber = " + CdsNumber + "]";
    }
}
