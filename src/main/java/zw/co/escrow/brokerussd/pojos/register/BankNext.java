package zw.co.escrow.brokerussd.pojos.register;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class BankNext {

    @JsonProperty("bank")
    private String bank;

    @JsonProperty("bank_name")
    private String bankName;

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    @JsonProperty("bank_name")
    public String getBankName() {
        return bankName;
    }

    @JsonProperty("bank_name")
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "BankNext{" +
                "bank='" + bank + '\'' +
                ", bankName='" + bankName + '\'' +
                '}';
    }
}
