package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
/**
 * @author Godwin Tavirimirwa
 */
@Entity
@Table(name = "subscriber_session")
public class SubscriberSession implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "mobile", length = 100, unique = true)
    private String mobile;

    @Column(name = "menu")
    private String menu;

    @Column(name = "root_menu")
    private String rootMenu;

    @Column(name = "registered")
    private Boolean registered;

    @Column(name = "transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @Column(name = "session_complete")
    private boolean sessionComplete;

    @Column(name = "amount")
    private String amount;

    @Column(name = "cds_number")
    private String cdsNumber;

    @Column(name = "current_page")
    private Integer currentPage;

    @Column(name = "on_cancel_orders")
    private Boolean onCancelOrders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public boolean isSessionComplete() {
        return sessionComplete;
    }

    public void setSessionComplete(boolean sessionComplete) {
        this.sessionComplete = sessionComplete;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRootMenu() {
        return rootMenu;
    }

    public void setRootMenu(String rootMenu) {
        this.rootMenu = rootMenu;
    }

    public String getCdsNumber() {
        return cdsNumber;
    }

    public void setCdsNumber(String cdsNumber) {
        this.cdsNumber = cdsNumber;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Boolean getOnCancelOrders() {
        return onCancelOrders;
    }

    public void setOnCancelOrders(Boolean onCancelOrders) {
        this.onCancelOrders = onCancelOrders;
    }
}
