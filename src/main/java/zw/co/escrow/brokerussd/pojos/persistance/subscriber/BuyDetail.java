package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class BuyDetail {

    @JsonProperty
    private String price;

    @JsonProperty
    private String company;

    @JsonProperty
    private String volume;

    @JsonProperty
    private String totalValue;

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getVolume ()
    {
        return volume;
    }

    public void setVolume (String volume)
    {
        this.volume = volume;
    }

    public String getTotalValue ()
    {
        return totalValue;
    }

    public void setTotalValue (String totalValue)
    {
        this.totalValue = totalValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [price = "+price+", company = "+company+", volume = "+volume+", totalValue = "+totalValue+"]";
    }
}
