package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class MyPortfolio {

    @JsonProperty
    private String totalportvalue;

    @JsonProperty
    private String uncleared;

    @JsonProperty
    private String counter;

    @JsonProperty
    private String prevprice;

    @JsonProperty
    private String currentprice;

    @JsonProperty
    private String net;

    @JsonProperty
    private String lastactivitydate;

    @JsonProperty
    private SellDetail[] SellDetail;

    @JsonProperty
    private BuyDetail[] BuyDetail;

    @JsonProperty
    private String id;

    @JsonProperty
    private String numbershares;

    @JsonProperty
    private String totalPrevPortValue;

    @JsonProperty
    private String returns;

    @JsonProperty
    private String companyFullName;

    public String getTotalportvalue() {
        return totalportvalue;
    }

    public void setTotalportvalue(String totalportvalue) {
        this.totalportvalue = totalportvalue;
    }

    public String getUncleared() {
        return uncleared;
    }

    public void setUncleared(String uncleared) {
        this.uncleared = uncleared;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getPrevprice() {
        return prevprice;
    }

    public void setPrevprice(String prevprice) {
        this.prevprice = prevprice;
    }

    public String getCurrentprice() {
        return currentprice;
    }

    public void setCurrentprice(String currentprice) {
        this.currentprice = currentprice;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getLastactivitydate() {
        return lastactivitydate;
    }

    public void setLastactivitydate(String lastactivitydate) {
        this.lastactivitydate = lastactivitydate;
    }

    public SellDetail[] getSellDetail() {
        return SellDetail;
    }

    public void setSellDetail(SellDetail[] SellDetail) {
        this.SellDetail = SellDetail;
    }

    public BuyDetail[] getBuyDetail() {
        return BuyDetail;
    }

    public void setBuyDetail(BuyDetail[] BuyDetail) {
        this.BuyDetail = BuyDetail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumbershares() {
        return numbershares;
    }

    public void setNumbershares(String numbershares) {
        this.numbershares = numbershares;
    }

    public String getTotalPrevPortValue() {
        return totalPrevPortValue;
    }

    public void setTotalPrevPortValue(String totalPrevPortValue) {
        this.totalPrevPortValue = totalPrevPortValue;
    }

    public String getReturns() {
        return returns;
    }

    public void setReturns(String returns) {
        this.returns = returns;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }

    @Override
    public String toString() {
        return "ClassPojo [totalportvalue = " + totalportvalue + ", " +
                "uncleared = " + uncleared + "," +
                " counter = " + counter + ", prevprice = " + prevprice + "," +
                " currentprice = " + currentprice + ", net = " + net + "," +
                " lastactivitydate = " + lastactivitydate + ", " +
                " id = " + id + ", numbershares = " + numbershares + "," +
                " totalPrevPortValue = " + totalPrevPortValue + ", " +
                "returns = " + returns + "," +
                " companyFullName = " + companyFullName + "]";
    }
}
