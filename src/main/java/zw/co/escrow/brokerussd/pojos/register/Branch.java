package zw.co.escrow.brokerussd.pojos.register;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class Branch {

    @JsonProperty
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "Branch [Name = " + Name + "]";
    }
}
