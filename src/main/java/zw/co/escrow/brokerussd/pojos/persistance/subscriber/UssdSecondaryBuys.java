/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tavman
 */
@Entity
@Table(name = "ussd_secondary_buys")
public class UssdSecondaryBuys implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "session_id")
    private String sessionId;
    @Column(name = "Msisdn")
    private String msisdn;
    @Column(name = "Pin")
    private String pin;
    @Column(name = "Bid")
    private String bid;
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "Sent")
    private Boolean sent;
    @Column(name = "Accepted")
    private Boolean accepted;
    @Column(name = "Reference")
    private String reference;
    @Column(name = "Success")
    private Boolean success;
    @Column(name = "Corfirmed")
    private Boolean corfirmed;
    @Column(name = "mobile_sent")
    private Integer mobileSent;
    @Column(name = "cds_number")
    private String cdsNumber;

    public UssdSecondaryBuys() {
    }

    public UssdSecondaryBuys(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getCorfirmed() {
        return corfirmed;
    }

    public void setCorfirmed(Boolean corfirmed) {
        this.corfirmed = corfirmed;
    }

    public Integer getMobileSent() {
        return mobileSent;
    }

    public void setMobileSent(Integer mobileSent) {
        this.mobileSent = mobileSent;
    }

    public String getCdsNumber() {
        return cdsNumber;
    }

    public void setCdsNumber(String cdsNumber) {
        this.cdsNumber = cdsNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UssdSecondaryBuys)) {
            return false;
        }
        UssdSecondaryBuys other = (UssdSecondaryBuys) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


}
