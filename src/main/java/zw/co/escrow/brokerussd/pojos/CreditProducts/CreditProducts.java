/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.CreditProducts;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tavman
 */
@Entity
@Table(name = "CreditProducts")
/*@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CreditProducts.findAll", query = "SELECT c FROM CreditProducts c")})*/
public class CreditProducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "ClientType")
    private Long clientType;
    @Column(name = "ProductType")
    private String productType;
    @Column(name = "DisplayName")
    private String displayName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MinAmt")
    private BigDecimal minAmt;
    @Column(name = "MaxAmt")
    private BigDecimal maxAmt;
    @Column(name = "MinIntRate")
    private BigDecimal minIntRate;
    @Column(name = "MaxIntRate")
    private BigDecimal maxIntRate;
    @Column(name = "DefaultIntRate")
    private BigDecimal defaultIntRate;
    @Column(name = "DefaultIntInterval")
    private String defaultIntInterval;
    @Column(name = "IntCalcMethod")
    private String intCalcMethod;
    @Column(name = "IntTrigger")
    private String intTrigger;
    @Column(name = "DaysInYear")
    private Integer daysInYear;
    @Column(name = "RepaymentFreq")
    private String repaymentFreq;
    @Column(name = "HasGracePeriod")
    private String hasGracePeriod;
    @Column(name = "GracePeriodType")
    private String gracePeriodType;
    @Column(name = "GracePeriodLength")
    private BigDecimal gracePeriodLength;
    @Column(name = "GracePeriodUnit")
    private String gracePeriodUnit;
    @Column(name = "AllowRepaymentOnWknd")
    private String allowRepaymentOnWknd;
    @Column(name = "IfRepaymentFallsOnWknd")
    private String ifRepaymentFallsOnWknd;
    @Column(name = "AllowEditingPaymentSchedule")
    private String allowEditingPaymentSchedule;
    @Column(name = "RepayOrder1")
    private String repayOrder1;
    @Column(name = "RepayOrder2")
    private String repayOrder2;
    @Column(name = "RepayOrder3")
    private String repayOrder3;
    @Column(name = "RepayOrder4")
    private String repayOrder4;
    @Column(name = "TolerancePeriodNum")
    private BigDecimal tolerancePeriodNum;
    @Column(name = "TolerancePeriodUnit")
    private String tolerancePeriodUnit;
    @Column(name = "ArrearNonWorkingDays")
    private String arrearNonWorkingDays;
    @Column(name = "PenaltyCharged")
    private String penaltyCharged;
    @Column(name = "PenaltyOption")
    private String penaltyOption;
    @Column(name = "AmtToPenalise")
    private String amtToPenalise;
    @Column(name = "ProductFees")
    private String productFees;
    @Column(name = "ProductFeeCalc")
    private String productFeeCalc;
    @Column(name = "ProductFeeAmtPerc")
    private BigDecimal productFeeAmtPerc;
    @Column(name = "CreatedBy")
    private String createdBy;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "ModifiedBy")
    private String modifiedBy;
    @Column(name = "ModifiedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    @Column(name = "LoanTenure")
    private Integer loanTenure;
    @Column(name = "RepaymentIntervalNum")
    private Integer repaymentIntervalNum;
    @Column(name = "RepaymentIntervalUnit")
    private String repaymentIntervalUnit;
    @Column(name = "MinimumTenure")
    private Integer minimumTenure;
    @Column(name = "MaximumTenure")
    private Integer maximumTenure;
    @Column(name = "DefaultTenure")
    private Integer defaultTenure;
    @Column(name = "PenaltyRate")
    private BigDecimal penaltyRate;
    @Column(name = "PenaltyInterval")
    private String penaltyInterval;
    @Column(name = "Active")
    private Boolean active;
    @Column(name = "DateCreated")
    private String dateCreated;
    @Column(name = "MinGracePerLength")
    private Integer minGracePerLength;
    @Column(name = "MaxGracePerLength")
    private Integer maxGracePerLength;
    @Column(name = "SalaryBasedLimit")
    private String salaryBasedLimit;
    @Basic(optional = false)
    @Column(name = "LimitBasedOn")
    private String limitBasedOn;
    @Column(name = "MaxRateAllowed")
    private BigDecimal maxRateAllowed;
    @Column(name = "ProductDesc")
    private String productDesc;
    @Column(name = "MinimumProductFeeAmount")
    private String minimumProductFeeAmount;
    @Column(name = "MaximumProductFeeAmount")
    private String maximumProductFeeAmount;
    @Column(name = "MinimumEstab")
    private String minimumEstab;
    @Column(name = "MaximumEstab")
    private String maximumEstab;

    public CreditProducts() {
    }

    public CreditProducts(Long id) {
        this.id = id;
    }

    public CreditProducts(Long id, String limitBasedOn) {
        this.id = id;
        this.limitBasedOn = limitBasedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientType() {
        return clientType;
    }

    public void setClientType(Long clientType) {
        this.clientType = clientType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public BigDecimal getMinAmt() {
        return minAmt;
    }

    public void setMinAmt(BigDecimal minAmt) {
        this.minAmt = minAmt;
    }

    public BigDecimal getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(BigDecimal maxAmt) {
        this.maxAmt = maxAmt;
    }

    public BigDecimal getMinIntRate() {
        return minIntRate;
    }

    public void setMinIntRate(BigDecimal minIntRate) {
        this.minIntRate = minIntRate;
    }

    public BigDecimal getMaxIntRate() {
        return maxIntRate;
    }

    public void setMaxIntRate(BigDecimal maxIntRate) {
        this.maxIntRate = maxIntRate;
    }

    public BigDecimal getDefaultIntRate() {
        return defaultIntRate;
    }

    public void setDefaultIntRate(BigDecimal defaultIntRate) {
        this.defaultIntRate = defaultIntRate;
    }

    public String getDefaultIntInterval() {
        return defaultIntInterval;
    }

    public void setDefaultIntInterval(String defaultIntInterval) {
        this.defaultIntInterval = defaultIntInterval;
    }

    public String getIntCalcMethod() {
        return intCalcMethod;
    }

    public void setIntCalcMethod(String intCalcMethod) {
        this.intCalcMethod = intCalcMethod;
    }

    public String getIntTrigger() {
        return intTrigger;
    }

    public void setIntTrigger(String intTrigger) {
        this.intTrigger = intTrigger;
    }

    public Integer getDaysInYear() {
        return daysInYear;
    }

    public void setDaysInYear(Integer daysInYear) {
        this.daysInYear = daysInYear;
    }

    public String getRepaymentFreq() {
        return repaymentFreq;
    }

    public void setRepaymentFreq(String repaymentFreq) {
        this.repaymentFreq = repaymentFreq;
    }

    public String getHasGracePeriod() {
        return hasGracePeriod;
    }

    public void setHasGracePeriod(String hasGracePeriod) {
        this.hasGracePeriod = hasGracePeriod;
    }

    public String getGracePeriodType() {
        return gracePeriodType;
    }

    public void setGracePeriodType(String gracePeriodType) {
        this.gracePeriodType = gracePeriodType;
    }

    public BigDecimal getGracePeriodLength() {
        return gracePeriodLength;
    }

    public void setGracePeriodLength(BigDecimal gracePeriodLength) {
        this.gracePeriodLength = gracePeriodLength;
    }

    public String getGracePeriodUnit() {
        return gracePeriodUnit;
    }

    public void setGracePeriodUnit(String gracePeriodUnit) {
        this.gracePeriodUnit = gracePeriodUnit;
    }

    public String getAllowRepaymentOnWknd() {
        return allowRepaymentOnWknd;
    }

    public void setAllowRepaymentOnWknd(String allowRepaymentOnWknd) {
        this.allowRepaymentOnWknd = allowRepaymentOnWknd;
    }

    public String getIfRepaymentFallsOnWknd() {
        return ifRepaymentFallsOnWknd;
    }

    public void setIfRepaymentFallsOnWknd(String ifRepaymentFallsOnWknd) {
        this.ifRepaymentFallsOnWknd = ifRepaymentFallsOnWknd;
    }

    public String getAllowEditingPaymentSchedule() {
        return allowEditingPaymentSchedule;
    }

    public void setAllowEditingPaymentSchedule(String allowEditingPaymentSchedule) {
        this.allowEditingPaymentSchedule = allowEditingPaymentSchedule;
    }

    public String getRepayOrder1() {
        return repayOrder1;
    }

    public void setRepayOrder1(String repayOrder1) {
        this.repayOrder1 = repayOrder1;
    }

    public String getRepayOrder2() {
        return repayOrder2;
    }

    public void setRepayOrder2(String repayOrder2) {
        this.repayOrder2 = repayOrder2;
    }

    public String getRepayOrder3() {
        return repayOrder3;
    }

    public void setRepayOrder3(String repayOrder3) {
        this.repayOrder3 = repayOrder3;
    }

    public String getRepayOrder4() {
        return repayOrder4;
    }

    public void setRepayOrder4(String repayOrder4) {
        this.repayOrder4 = repayOrder4;
    }

    public BigDecimal getTolerancePeriodNum() {
        return tolerancePeriodNum;
    }

    public void setTolerancePeriodNum(BigDecimal tolerancePeriodNum) {
        this.tolerancePeriodNum = tolerancePeriodNum;
    }

    public String getTolerancePeriodUnit() {
        return tolerancePeriodUnit;
    }

    public void setTolerancePeriodUnit(String tolerancePeriodUnit) {
        this.tolerancePeriodUnit = tolerancePeriodUnit;
    }

    public String getArrearNonWorkingDays() {
        return arrearNonWorkingDays;
    }

    public void setArrearNonWorkingDays(String arrearNonWorkingDays) {
        this.arrearNonWorkingDays = arrearNonWorkingDays;
    }

    public String getPenaltyCharged() {
        return penaltyCharged;
    }

    public void setPenaltyCharged(String penaltyCharged) {
        this.penaltyCharged = penaltyCharged;
    }

    public String getPenaltyOption() {
        return penaltyOption;
    }

    public void setPenaltyOption(String penaltyOption) {
        this.penaltyOption = penaltyOption;
    }

    public String getAmtToPenalise() {
        return amtToPenalise;
    }

    public void setAmtToPenalise(String amtToPenalise) {
        this.amtToPenalise = amtToPenalise;
    }

    public String getProductFees() {
        return productFees;
    }

    public void setProductFees(String productFees) {
        this.productFees = productFees;
    }

    public String getProductFeeCalc() {
        return productFeeCalc;
    }

    public void setProductFeeCalc(String productFeeCalc) {
        this.productFeeCalc = productFeeCalc;
    }

    public BigDecimal getProductFeeAmtPerc() {
        return productFeeAmtPerc;
    }

    public void setProductFeeAmtPerc(BigDecimal productFeeAmtPerc) {
        this.productFeeAmtPerc = productFeeAmtPerc;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getLoanTenure() {
        return loanTenure;
    }

    public void setLoanTenure(Integer loanTenure) {
        this.loanTenure = loanTenure;
    }

    public Integer getRepaymentIntervalNum() {
        return repaymentIntervalNum;
    }

    public void setRepaymentIntervalNum(Integer repaymentIntervalNum) {
        this.repaymentIntervalNum = repaymentIntervalNum;
    }

    public String getRepaymentIntervalUnit() {
        return repaymentIntervalUnit;
    }

    public void setRepaymentIntervalUnit(String repaymentIntervalUnit) {
        this.repaymentIntervalUnit = repaymentIntervalUnit;
    }

    public Integer getMinimumTenure() {
        return minimumTenure;
    }

    public void setMinimumTenure(Integer minimumTenure) {
        this.minimumTenure = minimumTenure;
    }

    public Integer getMaximumTenure() {
        return maximumTenure;
    }

    public void setMaximumTenure(Integer maximumTenure) {
        this.maximumTenure = maximumTenure;
    }

    public Integer getDefaultTenure() {
        return defaultTenure;
    }

    public void setDefaultTenure(Integer defaultTenure) {
        this.defaultTenure = defaultTenure;
    }

    public BigDecimal getPenaltyRate() {
        return penaltyRate;
    }

    public void setPenaltyRate(BigDecimal penaltyRate) {
        this.penaltyRate = penaltyRate;
    }

    public String getPenaltyInterval() {
        return penaltyInterval;
    }

    public void setPenaltyInterval(String penaltyInterval) {
        this.penaltyInterval = penaltyInterval;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getMinGracePerLength() {
        return minGracePerLength;
    }

    public void setMinGracePerLength(Integer minGracePerLength) {
        this.minGracePerLength = minGracePerLength;
    }

    public Integer getMaxGracePerLength() {
        return maxGracePerLength;
    }

    public void setMaxGracePerLength(Integer maxGracePerLength) {
        this.maxGracePerLength = maxGracePerLength;
    }

    public String getSalaryBasedLimit() {
        return salaryBasedLimit;
    }

    public void setSalaryBasedLimit(String salaryBasedLimit) {
        this.salaryBasedLimit = salaryBasedLimit;
    }

    public String getLimitBasedOn() {
        return limitBasedOn;
    }

    public void setLimitBasedOn(String limitBasedOn) {
        this.limitBasedOn = limitBasedOn;
    }

    public BigDecimal getMaxRateAllowed() {
        return maxRateAllowed;
    }

    public void setMaxRateAllowed(BigDecimal maxRateAllowed) {
        this.maxRateAllowed = maxRateAllowed;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getMinimumProductFeeAmount() {
        return minimumProductFeeAmount;
    }

    public void setMinimumProductFeeAmount(String minimumProductFeeAmount) {
        this.minimumProductFeeAmount = minimumProductFeeAmount;
    }

    public String getMaximumProductFeeAmount() {
        return maximumProductFeeAmount;
    }

    public void setMaximumProductFeeAmount(String maximumProductFeeAmount) {
        this.maximumProductFeeAmount = maximumProductFeeAmount;
    }

    public String getMinimumEstab() {
        return minimumEstab;
    }

    public void setMinimumEstab(String minimumEstab) {
        this.minimumEstab = minimumEstab;
    }

    public String getMaximumEstab() {
        return maximumEstab;
    }

    public void setMaximumEstab(String maximumEstab) {
        this.maximumEstab = maximumEstab;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CreditProducts)) {
            return false;
        }
        CreditProducts other = (CreditProducts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pojogenerator.CreditProducts[ id=" + id + " ]";
    }
    
}
