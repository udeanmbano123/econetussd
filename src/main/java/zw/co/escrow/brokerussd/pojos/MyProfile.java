package zw.co.escrow.brokerussd.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class MyProfile {

    private String idnopp;

    @JsonProperty
    private String Country;
    @JsonProperty
    private String Title;
    @JsonProperty
    private String Nationality;
    @JsonProperty
    private String Custodian;
    @JsonProperty
    private String CDS_Number;
    @JsonProperty
    private String mobile_number;
    @JsonProperty
    private String Email;
    @JsonProperty
    private String Cash_AccountNo;
    @JsonProperty
    private String BrokerCode;
    @JsonProperty
    private String Cash_Bank;
    @JsonProperty
    private String Add_1;
    @JsonProperty
    private String Mobile;
    @JsonProperty
    private String Cash_Branch;
    @JsonProperty
    private String Surname;
    @JsonProperty
    private String Forenames;

    public String getIdnopp() {
        return idnopp;
    }

    public void setIdnopp(String idnopp) {
        this.idnopp = idnopp;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String Nationality) {
        this.Nationality = Nationality;
    }

    public String getCustodian() {
        return Custodian;
    }

    public void setCustodian(String Custodian) {
        this.Custodian = Custodian;
    }

    public String getCDS_Number() {
        return CDS_Number;
    }

    public void setCDS_Number(String CDS_Number) {
        this.CDS_Number = CDS_Number;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getCash_AccountNo() {
        return Cash_AccountNo;
    }

    public void setCash_AccountNo(String Cash_AccountNo) {
        this.Cash_AccountNo = Cash_AccountNo;
    }

    public String getBrokerCode() {
        return BrokerCode;
    }

    public void setBrokerCode(String BrokerCode) {
        this.BrokerCode = BrokerCode;
    }

    public String getCash_Bank() {
        return Cash_Bank;
    }

    public void setCash_Bank(String Cash_Bank) {
        this.Cash_Bank = Cash_Bank;
    }

    public String getAdd_1() {
        return Add_1;
    }

    public void setAdd_1(String Add_1) {
        this.Add_1 = Add_1;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    public String getCash_Branch() {
        return Cash_Branch;
    }

    public void setCash_Branch(String Cash_Branch) {
        this.Cash_Branch = Cash_Branch;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getForenames() {
        return Forenames;
    }

    public void setForenames(String Forenames) {
        this.Forenames = Forenames;
    }

    @Override
    public String toString() {
        return "ClassPojo [idnopp = " + idnopp + ", Country = " + Country + ", Title = " + Title + ", Nationality = " + Nationality + ", Custodian = " + Custodian + ", CDS_Number = " + CDS_Number + ", mobile_number = " + mobile_number + ", Email = " + Email + ", Cash_AccountNo = " + Cash_AccountNo + ", BrokerCode = " + BrokerCode + ", Cash_Bank = " + Cash_Bank + ", Add_1 = " + Add_1 + ", Mobile = " + Mobile + ", Cash_Branch = " + Cash_Branch + ", Surname = " + Surname + ", Forenames = " + Forenames + "]";
    }

}
