package zw.co.escrow.brokerussd.pojos.persistance;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Godwin Tavirimirwa
 */
public class CompanySecurities {
    @JsonProperty
    private String IsinNo;
    @JsonProperty
    private String Id;
    @JsonProperty
    private String Instrument;
    @JsonProperty
    private String InitialPrice;

    public String getIsinNo() {
        return IsinNo;
    }

    public void setIsinNo(String IsinNo) {
        this.IsinNo = IsinNo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getInstrument() {
        return Instrument;
    }

    public void setInstrument(String Instrument) {
        this.Instrument = Instrument;
    }

    public String getInitialPrice() {
        return InitialPrice;
    }

    public void setInitialPrice(String InitialPrice) {
        this.InitialPrice = InitialPrice;
    }

    @Override
    public String toString() {
        return "CompanySecurities [IsinNo = " + IsinNo + ", Id = " + Id + ", Instrument = " + Instrument + ", InitialPrice = " + InitialPrice + "]";
    }
}
