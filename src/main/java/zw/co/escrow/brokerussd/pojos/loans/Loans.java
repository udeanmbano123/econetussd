/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.loans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Udean Mbano
 */
@Entity
@Table(name = "Loans")
public class Loans implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "Mobile")
    private String mobile;
    @Column(name = "ECNO")
    private String ecno;
    @Column(name = "IDNumber")
    private String iDNumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LoanAmount")
    private BigDecimal loanAmount;
    @Column(name = "Tenure")
    private BigDecimal tenure;
    @Column(name = "LoanDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loanDate;
    @Column(name = "LoanType")
    private String loanType;
    @Column(name = "Stage")
    private String stage;
    @Column(name = "SendTo")
    private String sendTo;
    @Column(name = "Disbursed")
    private Boolean disbursed;
    @Column(name = "DisbursedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date disbursedDate;
    @Column(name = "NetSalary")
    private BigDecimal netSalary;
    @Column(name = "Installment")
    private BigDecimal installment;
    public Loans() {
    }

    public Loans(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEcno() {
        return ecno;
    }

    public void setEcno(String ecno) {
        this.ecno = ecno;
    }

    public String getIDNumber() {
        return iDNumber;
    }

    public void setIDNumber(String iDNumber) {
        this.iDNumber = iDNumber;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public BigDecimal getTenure() {
        return tenure;
    }

    public void setTenure(BigDecimal tenure) {
        this.tenure = tenure;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }
    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getStage() {
        return stage;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }
    public String getSendToe() {
        return sendTo;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
    public Boolean getDisbursed() {
        return disbursed;
    }

    public void setDisbursed(Boolean disbursed) {
        this.disbursed = disbursed;
    }

    public Date getDisbursedDate() {
        return loanDate;
    }

    public void setDisbursedDate(Date disbursedDate) {
        this.disbursedDate = disbursedDate;
    }
    public BigDecimal getNetSalary() {
        return netSalary;
    }
    public void setNetSalary(BigDecimal netSalary) {
        this.netSalary = netSalary;
    }
    public BigDecimal getInstallment() {
        return installment;
    }
    public void setInstallment(BigDecimal installment) {
        this.installment = installment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loans)) {
            return false;
        }
        Loans other = (Loans) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pojo.Loans[ id=" + id + " ]";
    }

}
