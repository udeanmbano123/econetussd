/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.escrow.brokerussd.pojos.persistance.subscriber;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tavman
 */
@Entity
@Table(name = "trade_session")
public class TradeSession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "exchange")
    private String exchange;

    @Column(name = "order_type")
    private String orderType;

    @Column(name = "selected_company")
    private String selectedCompany;

    @Column(name = "time_in_force")
    private String timeInForce;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @Column(name = "session_complete")
    private Boolean sessionComplete;

    @Column(name = "last_company_index")
    private Integer lastCompanyIndex;

    @Column(name = "broker")
    private String broker;

    @Column(name = "broker_threshold")
    private String brokerThreshold;

    @Column(name = "current_price")
    private String currentPrice;

    @Column(name = "selected_price")
    private String selectedPrice;

    @Column(name = "cds_number")
    private String cdsNumber;

    @Column(name = "on_companies")
    private Boolean onCompanies;

    @Column(name = "on_brokers")
    private Boolean onBrokers;

    @Column(name = "current_page")
    private Integer currentPage;

    @Column(name = "on_zse_menu")
    private Boolean onZseMenu;

    @Column(name = "selected_row")
    private String selectedRow;

    @Column(name = "onentry")
    private Boolean onentry;

    public TradeSession() {
    }

    public TradeSession(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSelectedCompany() {
        return selectedCompany;
    }

    public void setSelectedCompany(String selectedCompany) {
        this.selectedCompany = selectedCompany;
    }

    public String getTimeInForce() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Boolean getSessionComplete() {
        return sessionComplete;
    }

    public void setSessionComplete(Boolean sessionComplete) {
        this.sessionComplete = sessionComplete;
    }

    public Integer getLastCompanyIndex() {
        return lastCompanyIndex;
    }

    public void setLastCompanyIndex(Integer lastCompanyIndex) {
        this.lastCompanyIndex = lastCompanyIndex;
    }

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getSelectedPrice() {
        return selectedPrice;
    }

    public void setSelectedPrice(String selectedPrice) {
        this.selectedPrice = selectedPrice;
    }

    public String getCdsNumber() {
        return cdsNumber;
    }

    public void setCdsNumber(String cdsNumber) {
        this.cdsNumber = cdsNumber;
    }

    public Boolean getOnCompanies() {
        return onCompanies;
    }

    public void setOnCompanies(Boolean onCompanies) {
        this.onCompanies = onCompanies;
    }

    public Boolean getOnBrokers() {
        return onBrokers;
    }

    public void setOnBrokers(Boolean onBrokers) {
        this.onBrokers = onBrokers;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Boolean getOnZseMenu() {
        return onZseMenu;
    }

    public void setOnZseMenu(Boolean onZseMenu) {
        this.onZseMenu = onZseMenu;
    }

    public String getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(String selectedRow) {
        this.selectedRow = selectedRow;
    }

    public Boolean getOnentry() {
        return onentry;
    }

    public void setOnentry(Boolean onentry) {
        this.onentry = onentry;
    }

    public String getBrokerThreshold() {
        return brokerThreshold;
    }

    public void setBrokerThreshold(String brokerThreshold) {
        this.brokerThreshold = brokerThreshold;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TradeSession)) {
            return false;
        }
        TradeSession other = (TradeSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public TradeSession(String mobile, String exchange,
                        String orderType, String selectedCompany,
                        String timeInForce, String quantity,
                        Date transactionDate, Boolean sessionComplete,
                        Integer lastCompanyIndex) {
        this.mobile = mobile;
        this.exchange = exchange;
        this.orderType = orderType;
        this.selectedCompany = selectedCompany;
        this.timeInForce = timeInForce;
        this.quantity = quantity;
        this.transactionDate = transactionDate;
        this.sessionComplete = sessionComplete;
        this.lastCompanyIndex = lastCompanyIndex;
    }

    @Override
    public String toString() {
        return "TradeSession{" +
                "id=" + id +
                ", mobile='" + mobile + '\'' +
                ", exchange='" + exchange + '\'' +
                ", orderType='" + orderType + '\'' +
                ", selectedCompany='" + selectedCompany + '\'' +
                ", timeInForce='" + timeInForce + '\'' +
                ", quantity='" + quantity + '\'' +
                ", transactionDate=" + transactionDate +
                ", sessionComplete=" + sessionComplete +
                ", lastCompanyIndex=" + lastCompanyIndex +
                ", broker='" + broker + '\'' +
                ", currentPrice='" + currentPrice + '\'' +
                ", selectedPrice='" + selectedPrice + '\'' +
                ", cdsNumber='" + cdsNumber + '\'' +
                ", onCompanies=" + onCompanies +
                ", onBrokers=" + onBrokers +
                ", currentPage=" + currentPage +
                ", onZseMenu=" + onZseMenu +
                ", selectedRow='" + selectedRow + '\'' +
                ", onentry=" + onentry +
                '}';
    }
}
