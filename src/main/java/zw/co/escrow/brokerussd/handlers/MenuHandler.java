package zw.co.escrow.brokerussd.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import zw.co.escrow.brokerussd.constants.NumbersEnum;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.TradeSession;
import zw.co.escrow.brokerussd.services.*;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;
import zw.co.escrow.brokerussd.services.loans.Loans;
import zw.co.escrow.brokerussd.services.myaccount.Myaccount;
import zw.co.escrow.brokerussd.services.trade.TradeService;

/**
 * @author Godwin Tavirimirwa
 */
@RestController
@RequestMapping("/broker-ussd-econet/api/escrow/econet-ussd/")
public class MenuHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuHandler.class);
    private InitialMenuService initialMenuService;
    private RegisterService registerService;
    private SubscriberSessionRepository subscriberSessionRepository;
    private RegisteredSubscriberService registeredSubscriberService;
    private GenericService genericService;
    private TradeService tradeService;
    private TradeSession tradeSession;
    private Loans myloanservice;
    private Myaccount myaccs;

    @Autowired
    public MenuHandler(InitialMenuService initialMenuService,
                       RegisterService registerService,
                       SubscriberSessionRepository subscriberSessionRepository,
                       RegisteredSubscriberService registeredSubscriberService,
                       GenericService genericService,
                       Loans loanService, Myaccount myaccs) {
        this.initialMenuService = initialMenuService;
        this.registerService = registerService;
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.registeredSubscriberService = registeredSubscriberService;
        this.genericService = genericService;
        this.tradeService = tradeService;
        this.myloanservice = loanService;
        this.myaccs = myaccs;
    }

    @PostMapping(
            value = "/initial/menu",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.ALL_VALUE},
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.ALL_VALUE})
    public @ResponseBody
    MessageResponseType initialMenu(@RequestBody MessageRequestType request) {

        SubscriberSession loggedSubscriber = subscriberSessionRepository.
                findByMobile(request.getSourceNumber());
        tradeSession = genericService.getTradeSession(request);

        MessageResponseType response;

        String message = request.getMessage();

        String stage = request.getStage();

        LOGGER.info("ECONET REQUEST: {}, {}", request,
                request.getMessage());

        if (loggedSubscriber == null) {

            response = this.initialMenuService.getInitialMenu(request);

        } else {

            String menu = loggedSubscriber.getMenu();
            String rootMenu = loggedSubscriber.getRootMenu();

            if (loggedSubscriber.getRegistered()) {

                if (stage.equalsIgnoreCase(StringsEnum.reqFirst.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.pinMenu.toString()) |
                        (menu.equalsIgnoreCase(StringsEnum.repayments.toString()) &&
                                message.equalsIgnoreCase(NumbersEnum.one.toString()))) {

                    response = this.initialMenuService.getInitialMenu(request);

                } else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.one.toString())) |
                        rootMenu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())
                        ) {

                    response = this.myloanservice.loansDispatcherMenu(request);

                } else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.two.toString())) |
                        rootMenu.equalsIgnoreCase(StringsEnum.AccSubMenu.toString())) {

                    response = this.myaccs.myaccountDispatcherMenu(request);

                }else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.four.toString())) |
                        rootMenu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())
                        ) {

                    response = this.myloanservice.loansDispatcherMenu(request);

                }else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.three.toString())) |
                        rootMenu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())
                        ) {

                    response = this.myloanservice.loansDispatcherMenu(request);

                } else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.five.toString())) |
                        (menu.equalsIgnoreCase(StringsEnum.repayments.toString()) &&
                                message.equalsIgnoreCase(NumbersEnum.zero.toString()))
                        ) {

                    response = this.genericService.exitUssd(request,"MAINMENU");

                } else if ((menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.five.toString())) |
                        (menu.equalsIgnoreCase(StringsEnum.repayments.toString()) &&
                                message.equalsIgnoreCase(NumbersEnum.two.toString()))
                        ) {

                    response = this.genericService.exitUssd(request,"MAINMENU");

                }  else {

                    response = this.genericService.invalidInput(request);
                }

            } else {

                if (stage.equalsIgnoreCase(StringsEnum.reqFirst.toString())) {

                    response = this.initialMenuService.getInitialMenu(request);

                } else if ((menu.equalsIgnoreCase(StringsEnum.unRegOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.one.toString())) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regSubMenu.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regPersonalDetails.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regNameDetails.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regBankDetails.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regAccount.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regAddresses.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.regCustodianMenu.toString()) |
                        rootMenu.equalsIgnoreCase(StringsEnum.existsAlready.toString())
                        ) {

                    response = this.registerService.registerDispatcherMenu(request);

                } else if (menu.equalsIgnoreCase(StringsEnum.unRegOne.toString()) &
                        message.equalsIgnoreCase(NumbersEnum.two.toString())) {

                    // response = this.myloanservice.loansDispatcherMenu(request);
                    response = this.genericService.exitUssd(request,"MAINMENU");
                } else {

                    LOGGER.info("Invalid in MenuHandler");

                    response = this.genericService.invalidInput(request);
                }

            }

        }

        LOGGER.info("RESPONSE {}", response);

        return response;
    }

}

