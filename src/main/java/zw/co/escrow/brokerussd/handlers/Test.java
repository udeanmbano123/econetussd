package zw.co.escrow.brokerussd.handlers;



import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;
import com.twilio.*;
import com.twilio.converter.*;
import com.twilio.rest.api.v2010.account.*;
import com.twilio.type.*;
import com.twilio.Twilio;
import com.twilio.converter.Promoter;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import zw.co.escrow.brokerussd.pojos.ApiCreds.ApiCredentials;

/**
 * @author Godwin Tavirimirwa
 */
public class Test {


        public static void main(String[] args) throws Exception {


            final String ACCOUNT_SID = "ACb039356b48f7e1357e744ff67d7db32f";
            final String AUTH_TOKEN = "64644c420ef1e02ca43b8c1d90ca4c68";
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
            Message message = Message.creator(
                    new com.twilio.type.PhoneNumber("+263779627951"),
                    new com.twilio.type.PhoneNumber("UntuCapital"),
                    "Your message bra we  made it")
                    .create();

            System.out.println(message.getSid());


             /*  String name = "escrowsys";
               String password ="Password123456@";
               String input="4000152Q";
            String authString = name + ":" + password;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            try {


                URL obj = new URL("https://untucapital.sandbox.mambu.com/api/clients/" + input + "/loans?accountState=ACTIVE");
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestProperty("Accept", "application/json");
                con.setRequestProperty("Authorization", "Basic " + authStringEnc);
                con.setRequestMethod("GET");
                int responseCode = con.getResponseCode();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                String jsonString = response.toString();
                float totalBalance=0;
             try {
                    JSONArray objs = new JSONArray(jsonString); // parse the array
                    for(int i = 0; i < objs.length(); i++){ // iterate over the arrayprincipalBalance
                        JSONObject o = objs.getJSONObject(i);
                        String id = o.getString("id");
                        String principalBalance = o.getString("principalBalance");
                        String accountState=o.getString("accountState");
                        double pBalance = Double.parseDouble(principalBalance);
                        if(accountState.equalsIgnoreCase("ACTIVE_IN_ARREARS")){
                            totalBalance+=pBalance;
                        }
                        if(accountState.equalsIgnoreCase("ACTIVE")){
                            totalBalance+=pBalance;
                        }
                    }
                } catch (JSONException e){
                   System.out.println(e.getMessage());
                }
                System.out.println("===Total balance is===: $ZWL" + totalBalance);

            } catch (Exception f) {


            }
*/

        }
   public static int roundDown(double number, double place) {
        double result = number / place;
        result = Math.floor(result);
        result *= place;
        return (int)result;
    }
    public static  String decode(String url)
    {
        try {
            String prevURL="";
            String decodeURL=url;
            while(!prevURL.equals(decodeURL))
            {
                prevURL=decodeURL;
                decodeURL=URLDecoder.decode( decodeURL, "UTF-8" );
            }
            return decodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while decoding" +e.getMessage();
        }
    }
    public static  String encode(String url)
    {
        try {
            String encodeURL=URLEncoder.encode( url, "UTF-8" );
            return encodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while encoding" +e.getMessage();
        }
    }

    public static boolean isNullOrEmpty(String str) {
        if(str.trim().length()<1)
            return true;


        return false;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
