package zw.co.escrow.brokerussd.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zw.co.escrow.brokerussd.constants.MultiDataTypesFormatter;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.pojos.*;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.MyPortfolio;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
import zw.co.escrow.brokerussd.pojos.register.MyOrders;
import zw.co.escrow.brokerussd.services.GenericService;
import zw.co.escrow.brokerussd.services.RegisteredSubscriberService;

import java.util.List;

/**
 * @author Udean Mbano
 */
@Service
public class RegisteredSubscriberServiceImpl implements RegisteredSubscriberService {

    private SubscriberSessionRepository subscriberSessionRepository;
    private GenericService genericService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisteredSubscriberServiceImpl.class);
    SubscriberSession session = null;
    private MessageResponseType messageResponseType;

    @Autowired
    public RegisteredSubscriberServiceImpl(SubscriberSessionRepository subscriberSessionRepository,
                                           GenericService genericService) {
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.genericService = genericService;
        this.messageResponseType = new MessageResponseType();
    }

    @Override
    public MessageResponseType getMyProfile(MessageRequestType request) {
        session = genericService.getSubscriberSession(request);
        MessageResponseType response = new MessageResponseType();
        List<MyProfile> subscriber = getMyProfileList(request);
        genericService.setFinalResponse(request, response);
        response.setMessage("Names: " + subscriber.get(0).getSurname() + " "
                + subscriber.get(0).getForenames() + "\n" +
                "CdsNumber: " + subscriber.get(0).getCDS_Number() + "\n" +
                "Bank: " + subscriber.get(0).getCash_Bank() + "\n" +
                "Custodian: " + subscriber.get(0).getCustodian() + "\n" +
                "Id Number: " + subscriber.get(0).getIdnopp() + "\n" +
                "Account No: " + subscriber.get(0).getCash_AccountNo());

        session = genericService.getSubscriberSession(request);
        session.setId(session.getId());
        session.setSessionComplete(true);
        subscriberSessionRepository.save(session);

        return response;
    }


    @Override
    public MessageResponseType getMyFunds(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType);
        messageResponseType.setMessage("Please Select:\n" +
                "1.Deposit\n" +
                "2.Withdraw\n" +
                "3.My Balance\n" +
                "4.My Transactions\n");

        session = genericService.getSubscriberSession(request);
        session.setId(session.getId());
        session.setMenu(StringsEnum.myFundsOne.toString());
        session.setSessionComplete(false);
        subscriberSessionRepository.save(session);
        return messageResponseType;
    }


    @Override
    public MessageResponseType getMyPortfolio(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        genericService.setFinalResponse(request, response);

        List<MyPortfolio> myPortfolio = getMyPortfolioList(request);
        StringBuffer buffer = new StringBuffer();

        int max = 5;

        if (myPortfolio.size() < max)
            max = myPortfolio.size();

        if (myPortfolio.size() < 1) {
            buffer.append("You do not have any portfolio values.");
        } else {
            for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

                buffer.append(itemNumber + ". " + myPortfolio.get(index).getCompanyFullName() + ": " +
                        " : "
                        + myPortfolio.get(index).getTotalportvalue() + "\n");
            }
        }

        response.setMessage(buffer.toString());

        session = genericService.getSubscriberSession(request);
        session.setId(session.getId());
        session.setSessionComplete(true);
        subscriberSessionRepository.save(session);
        return response;
    }

    @Override
    public MessageResponseType getMyOrders(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        genericService.setFinalResponse(request, response);

        List<MyOrders> ordersList = getMyOrdersList(request);
        StringBuffer buffer = new StringBuffer();

        if (ordersList == null || ordersList.isEmpty()) {

            buffer.append("You do not have any orders yet.");

        } else {

            int max = 3;

            if (ordersList.size() < 3)
                max = ordersList.size();

            for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

                buffer.append(itemNumber + ". " + ordersList.get(index).getFullname() + "(" +
                        ordersList.get(index).getType() + ") : " + ordersList.get(index).getStatus() + "\n");
            }

        }

        response.setMessage(buffer.toString());
        session = genericService.getSubscriberSession(request);
        session.setId(session.getId());
        session.setSessionComplete(true);
        subscriberSessionRepository.save(session);
        return response;
    }

    @Override
    public MessageResponseType getWithdrawalAmount(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType);
        messageResponseType.setMessage("Please Enter Withdrawal Amount:");

        session = genericService.getSubscriberSession(request);
        session.setId(session.getId());
        session.setMenu(StringsEnum.withdrawAmount.toString());
        session.setRootMenu(StringsEnum.rootWithdrawal.toString());
        session.setSessionComplete(false);
        subscriberSessionRepository.save(session);
        return messageResponseType;
    }

    @Override
    public MessageResponseType getConfirmWithdrawal(MessageRequestType request) {
        SubscriberSession session = subscriberSessionRepository.findByMobile(request.getSourceNumber());

        String message = request.getMessage();

        session.setId(session.getId());
        session.setMenu(StringsEnum.withdrawConfirm.toString());
        session.setAmount(message);
        session.setSessionComplete(false);
        subscriberSessionRepository.save(session);

        Double withdrawAmount = Double.valueOf(message);

        if (withdrawAmount < 10) {

            this.genericService.setFinalResponse(request, messageResponseType);
            messageResponseType.setMessage("Please minimum withdrawal amount is $10.");

        } else {

            this.genericService.setGenericResponse(request, messageResponseType);
            messageResponseType.setMessage("Are you sure you want to withdraw $" + session.getAmount() + " from C-Trade?\n" +
                    "1. Yes\n" +
                    "2. No");

        }

        return messageResponseType;
    }

    @Override
    public MessageResponseType getCompleteWithdrawal(MessageRequestType request) {
        SubscriberSession session = genericService.getSubscriberSession(request);
        genericService.setFinalResponse(request, messageResponseType);

        String message = request.getMessage();

        if (message.equalsIgnoreCase("1")) {

            if (this.sendWithdrawRequest(request).equalsIgnoreCase("200")) {
                messageResponseType.setMessage("Your withdrawal of $" + session.getAmount() + " " +
                        "from C-Trade was successful.");
                session.setId(session.getId());
                session.setSessionComplete(true);
                subscriberSessionRepository.save(session);
            } else {
                messageResponseType.setMessage("Withdrawal of " + session.getAmount() + " " +
                        "was unsuccessful. Please try again later.");
            }

        } else if (message.equalsIgnoreCase("2")) {

            messageResponseType.setMessage("You cancelled the withdrawal process.");

        } else {

            messageResponseType.setMessage("You provided an invalid input. Please try again later.");
        }

        return messageResponseType;
    }


    private List<MyOrders> getMyOrdersList(MessageRequestType request) {
        final String uri = StringsEnum.mobileApiUrl.toString() + "GetMyOrders1?cdsNumber="
                + genericService.getCdsNumber(request);
        RestTemplate template = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        List<MyOrders> myOrdersList = null;

        try {
            myOrdersList = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, MyOrders.class));
        } catch (Exception e) {
            LOGGER.info("Error my orders  {}", e.toString());
        }

        return myOrdersList;
    }


    private List<MyProfile> getMyProfileList(MessageRequestType request) {
        final String uri = StringsEnum.mobileApiUrl.toString() + "getAllDetails?cdsNumber="
                + genericService.getCdsNumber(request);
        RestTemplate template = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();

        List<MyProfile> profileList = null;

        try {
            profileList = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, MyProfile.class));
        } catch (Exception e) {
            LOGGER.info("Error  {}", e.toString());
        }

        return profileList;
    }

    private List<MyPortfolio> getMyPortfolioList(MessageRequestType request) {
        final String uri = StringsEnum.mobileApiUrl.toString()
                + "getMyPortFolio1?cdsNumber="
                + genericService.getCdsNumber(request);
        RestTemplate template = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();

        List<MyPortfolio> myPortfolioList = null;

        try {
            myPortfolioList = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, MyPortfolio.class));
        } catch (Exception e) {
            LOGGER.info("Error  {}", e.toString());
        }

        return myPortfolioList;
    }



    private String sendWithdrawRequest(MessageRequestType messageRequestType) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        SubscriberSession subscriberSession = genericService.getSubscriberSession(messageRequestType);

        final String url = StringsEnum.apiUrl.toString() + "Widthdraw";

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("cdsnumber", genericService.getCdsNumber(messageRequestType));
        map.add("ammount", subscriberSession.getAmount());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

        LOGGER.info("Withdrawal response {}", response.toString());

        return response.getStatusCode().toString();
    }

}
