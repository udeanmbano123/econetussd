package zw.co.escrow.brokerussd.services.loans;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface Loans {

    MessageResponseType loansDispatcherMenu(MessageRequestType messageRequestType);

    MessageResponseType getLoansMenu(MessageRequestType request);
    MessageResponseType getTenure(MessageRequestType request);
    MessageResponseType getTenure2(MessageRequestType request);
    MessageResponseType getLoanAmount(MessageRequestType request);
    MessageResponseType getLoanAmount2(MessageRequestType request);

    MessageResponseType getLoanAmountEmergency(MessageRequestType request);
    MessageResponseType confirmNameDetails(MessageRequestType request);
    MessageResponseType nameDetailsComplete(MessageRequestType request);
    MessageResponseType LoanAuthorization(MessageRequestType request);
    MessageResponseType getLoanTracker(MessageRequestType request);

    MessageResponseType getTenureVal(MessageRequestType request,String message);
    MessageResponseType getLoanAmountVal(MessageRequestType request,String message);
    MessageResponseType getNetSalary(MessageRequestType request);
    MessageResponseType getNetSalary2(MessageRequestType request);
    MessageResponseType getNetSalaryVal(MessageRequestType request,String message);
    int roundDown(double number, double place);
    MessageResponseType confirmNameDetails2(MessageRequestType request);
    MessageResponseType confirmNameDetails3(MessageRequestType request);
    MessageResponseType getLoanAmountSSB(MessageRequestType request);
    MessageResponseType getLoanAmountValSSB(MessageRequestType request,String message);
    public void getFinalInstallmentSSB(MessageRequestType request,double loanamounts);

}
