package zw.co.escrow.brokerussd.services;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface RegisterService {

    MessageResponseType registerDispatcherMenu(MessageRequestType messageRequestType);

    MessageResponseType getRegisterMenu(MessageRequestType request);

    MessageResponseType getTitle(MessageRequestType request);

    MessageResponseType getSurname(MessageRequestType request);

    MessageResponseType getName(MessageRequestType request);

    MessageResponseType confirmNameDetails(MessageRequestType request);

    MessageResponseType confirmMenuTwoDetails(MessageRequestType request);

    MessageResponseType getNationalId(MessageRequestType request);

    MessageResponseType getNationality(MessageRequestType request);

    MessageResponseType getDob(MessageRequestType request);

    MessageResponseType getAddress(MessageRequestType request);

    MessageResponseType getGender(MessageRequestType request);

    MessageResponseType getECNum(MessageRequestType request);
    MessageResponseType getECNumm(MessageRequestType request);
    MessageResponseType getMobile(MessageRequestType request);
    MessageResponseType getBank(MessageRequestType request);
    MessageResponseType getECNumVal(MessageRequestType request,String validation);
    MessageResponseType getBanksNext(MessageRequestType request);
    MessageResponseType getECNumSSB(MessageRequestType request);
    MessageResponseType getMambu(MessageRequestType request);
    MessageResponseType getSurnameOrdinary(MessageRequestType request);
    MessageResponseType getBranch(MessageRequestType request);

    MessageResponseType getAccountNo(MessageRequestType request);
    MessageResponseType getClientMenu(MessageRequestType request);
    MessageResponseType getLinkMenu(MessageRequestType request);
    MessageResponseType getEmail(MessageRequestType request);

    MessageResponseType getPassword(MessageRequestType request);

    MessageResponseType getConfirmPassword(MessageRequestType request);

    MessageResponseType getPin(MessageRequestType request);

    MessageResponseType getPin2(MessageRequestType request);

    MessageResponseType getConfirmPin(MessageRequestType request);

    MessageResponseType getRegisterConfirm(MessageRequestType request);

    MessageResponseType getRegisterComplete(MessageRequestType request);

    MessageResponseType personalDetailsComplete(MessageRequestType request);

    MessageResponseType custodianRegisterComplete(MessageRequestType request);

    MessageResponseType nameDetailsComplete(MessageRequestType request);

    MessageResponseType bankDetailsComplete(MessageRequestType request);

    MessageResponseType addressesComplete(MessageRequestType request);

    MessageResponseType menuAlreadyRegistered(MessageRequestType request, String menu);

    MessageResponseType alreadyRegistered(MessageRequestType request);
    MessageResponseType getPinVal(MessageRequestType request,String message);
    MessageResponseType saveUserToDatabase(MessageRequestType request);
    boolean isExistingAccount(String input);
    boolean isRegisteredAccount(String input);
    boolean isExistingAccountMambu(String input);
    String isExistingAccountMambuLoan(String input);

}
