package zw.co.escrow.brokerussd.services.trade;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface TradeService {

    MessageResponseType getMain(MessageRequestType request);

    MessageResponseType getCounters(MessageRequestType request);

    MessageResponseType getMarket(MessageRequestType request);

    MessageResponseType getTradeType(MessageRequestType request);

    MessageResponseType getCounterMenu(MessageRequestType request);

    MessageResponseType getMarketCounters(MessageRequestType request);

    MessageResponseType getSelectedCounterDetails(MessageRequestType request);

    MessageResponseType getQuantity(MessageRequestType request);

    MessageResponseType getZseMenu(MessageRequestType request);

    MessageResponseType getTradePrice(MessageRequestType request);

    MessageResponseType getBroker(MessageRequestType request);

    MessageResponseType getConfirmOrder(MessageRequestType request);

    MessageResponseType getOrderComplete(MessageRequestType request);
}
