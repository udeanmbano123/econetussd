package zw.co.escrow.brokerussd.services;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface RegisteredSubscriberService {

    MessageResponseType getMyProfile(MessageRequestType request);


    MessageResponseType getMyFunds(MessageRequestType request);


    MessageResponseType getMyPortfolio(MessageRequestType request);

    MessageResponseType getMyOrders(MessageRequestType request);

    MessageResponseType getWithdrawalAmount(MessageRequestType request);

    MessageResponseType getConfirmWithdrawal(MessageRequestType request);

    MessageResponseType getCompleteWithdrawal(MessageRequestType request);


}
