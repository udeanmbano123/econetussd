package zw.co.escrow.brokerussd.services.impl.loans;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.escrow.brokerussd.constants.MultiDataTypesFormatter;
import zw.co.escrow.brokerussd.constants.NumbersEnum;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.CreditProducts.CreditProductsRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.LoansRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.RegisterSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.pojos.CreditProducts.CreditProducts;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.RegisterSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.Subscribers;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.UssdMobile;
import zw.co.escrow.brokerussd.pojos.register.Bank;
import zw.co.escrow.brokerussd.pojos.register.BankNext;
import zw.co.escrow.brokerussd.pojos.register.Custodian;
import zw.co.escrow.brokerussd.services.GenericService;
import zw.co.escrow.brokerussd.services.InitialMenuService;
import zw.co.escrow.brokerussd.services.impl.RegisterServiceImpl;
import zw.co.escrow.brokerussd.pojos.loans.Loans;

import javax.persistence.Convert;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Udean Mbano
 */
@Service
public class LoansMenuImpl implements zw.co.escrow.brokerussd.services.loans.Loans {

    private GenericService genericService;
    private InitialMenuService initialMenuService;
    private List<String> titles,
            gender,
            nationalities;
    private List<Custodian> custodians;
    private List<Bank> banks;
    private List<BankNext> banksNext;
    private RegisterSession registerSession;
    private SubscriberSession subscriberSession;
    private RegisterSessionRepository registerSessionRepository;
    private SubscriberSessionRepository subscriberSessionRepository;
    private MessageResponseType messageResponseType;
    private LoansRepository loansRepository;
    private CreditProductsRepository creditProductsRepository;
    public Loans loans;
    public Double maxLoanAmt;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoansMenuImpl.class);

    @Autowired
    public LoansMenuImpl(InitialMenuService initialMenuService,GenericService genericService,
                               RegisterSessionRepository registerSessionRepository,
                               SubscriberSessionRepository subscriberSessionRepository,LoansRepository loansRepository,CreditProductsRepository creditProductsRepository) {
        this.genericService = genericService;
        this.registerSessionRepository = registerSessionRepository;
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.messageResponseType = new MessageResponseType();
        this.titles = new ArrayList<>();
        this.nationalities = new ArrayList<>();
        this.gender = new ArrayList<>();
        this.custodians = new ArrayList<>();
        this.banks = new ArrayList<>();
        this.banksNext = new ArrayList<>();
        this.loansRepository=loansRepository;
        this.initialMenuService = initialMenuService;
        this.creditProductsRepository=creditProductsRepository;
    }


    @Override
    public MessageResponseType loansDispatcherMenu(MessageRequestType request) {

        MessageResponseType response = null;

        subscriberSession = genericService.getSubscriberSession(request);

        String message = request.getMessage();
        String menu = subscriberSession.getMenu();
        String rootMenu = subscriberSession.getRootMenu();
        System.out.println("Root menu "+rootMenu);
        System.out.println("Menu "+menu);
    if (menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) && request.getMessage().equalsIgnoreCase(NumbersEnum.one.toString())){

        if(this.genericService.isAuthAccount(request.getSourceNumber())) {
            response = getLoansMenu(request);
        }else{
            response=LoanAuthorization(request);
        }
        }else if (menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) && message.equalsIgnoreCase(NumbersEnum.four.toString()) ) {

                    if(this.genericService.isAuthAccount(request.getSourceNumber())) {
                        loans = new Loans();
                        response = getLoanAmountEmergency(request);
                    }else{
                        response=LoanAuthorization(request);
                    }
        }else if (menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) && message.equalsIgnoreCase(NumbersEnum.three.toString()) ) {


                    if(this.genericService.isAuthAccount(request.getSourceNumber())) {
                     response = getLoanTracker(request);
                    }else{
                        response=LoanAuthorization(request);
                    }
        }else if (menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString()) && message.equalsIgnoreCase(NumbersEnum.two.toString()) ) {

        response = this.genericService.exitUssd(request,"LOANS");
    }else if (rootMenu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString()))
        {
            if (message.equalsIgnoreCase(NumbersEnum.one.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString()) ) {
                MultiDataTypesFormatter.noSpecialChars(request.getMessage());
                if(this.genericService.getSSB(request).equalsIgnoreCase("SSB")) {
                    response = getNetSalary(request);
                }else if(this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB")) {
                    response = getNetSalary(request);
                }else{
                    response = getLoanAmount(request);
                }
            } else if (message.equalsIgnoreCase(NumbersEnum.two.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())  ) {
                if(this.genericService.getSSB(request).equalsIgnoreCase("SSB")) {
                    response = getNetSalary2(request);
                }else if(this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB")) {
                    response = getNetSalary2(request);
                }else{
                    response = getLoanAmount2(request);
                }



            } else if (message.equalsIgnoreCase(NumbersEnum.two.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())  ) {
                response = getLoanTracker(request);

            }else if (message.equalsIgnoreCase(NumbersEnum.two.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.regTenure.toString()) ) {
                response = getTenure(request);

            } else if (message.equalsIgnoreCase(NumbersEnum.three.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.loanSubMenu.toString())  ) {
                response = getLoanTracker(request);

            }else if ( menu.equalsIgnoreCase(StringsEnum.regTenure2.toString()) ) {
                double deduct=0;
                double loanamount=0;
                try{
                    loanamount= Double.parseDouble(request.getMessage().toString());

                    deduct=loanamount-150;
                }catch(Exception f){

                }
                List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
                maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());
                if(this.genericService.isNumeric(request.getMessage())==false){
                    response=getNetSalaryVal(request,"Amount must be numeric");
                }else if(deduct<18){
                    response=getNetSalaryVal(request,"NetSalary deductible instalment disapproved");
                }else{
                    response = getTenure2(request);
                }


            }else if ( menu.equalsIgnoreCase(StringsEnum.regTenure.toString()) ) {
                double amount=0;
                try{
                    amount=Double.parseDouble(request.getMessage());
                }catch(Exception f){

                }
                List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
                maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());
               if(this.genericService.isNumeric(request.getMessage())==false){
                   response=getLoanAmountVal(request,"Amount must be numeric");
               }else if(amount>maxLoanAmt){
                   response=getLoanAmountVal(request,"Amount must not be greater than maximum loan amount (ZWL"+maxLoanAmt+")");
               }else{
                   response = getTenure(request);
               }


            } else if (menu.equalsIgnoreCase(StringsEnum.confirmCompleteLon.toString())) {
                double tenure=0;
                try
                {
                    tenure=Double.parseDouble(request.getMessage().toString());

                }catch(Exception f){

                }
                if(this.genericService.isNumeric(request.getMessage())==false){
                    response=getTenureVal(request,"Tenure must be numeric ");
                }else if((tenure>12) && (!loans.getLoanType().equalsIgnoreCase("Emergency Loan"))){
                    response=getTenureVal(request,"Tenure must not exceed 12");
                }else if((tenure<3) && (!loans.getLoanType().equalsIgnoreCase("Emergency Loan"))){
                    response=getTenureVal(request,"Tenure must be between 3 and 12");
                }else if((tenure>2) && (loans.getLoanType().equalsIgnoreCase("Emergency Loan"))){
                    response=getTenureVal(request,"Tenure must be between 1 and 2");
                }else{
                    response = confirmNameDetails(request);
                }


            }else if (menu.equalsIgnoreCase(StringsEnum.regConfirmLoanAmount.toString())) {

                    if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {
                        response = getLoanAmountSSB(request);
                }else if(message.equalsIgnoreCase(NumbersEnum.two.toString())) {
                    response = getLoansMenu(request);

                }else if(message.equalsIgnoreCase(NumbersEnum.three.toString())) {
                    subscriberSession.setId(subscriberSession.getId());
                    subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                    subscriberSession.setMenu(StringsEnum.repayments.toString());
                    subscriberSessionRepository.save(subscriberSession);
                    response = this.initialMenuService.getInitialMenu(request);

                } else if(message.equalsIgnoreCase(NumbersEnum.three.toString()) && !(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))) {
                    subscriberSession.setId(subscriberSession.getId());
                    subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                    subscriberSession.setMenu(StringsEnum.repayments.toString());
                    subscriberSessionRepository.save(subscriberSession);
                    response = this.initialMenuService.getInitialMenu(request);


                }

                     }
            else if (menu.equalsIgnoreCase(StringsEnum.regConfirmLoanCommit.toString())) {
                double amount=0;
                try
                {
                    amount=Double.parseDouble(request.getMessage().toString());

                }catch(Exception f){

                }
                double maxloanAmount= Double.parseDouble(loans.getLoanAmount().toString());
                if(this.genericService.isNumeric(request.getMessage())==false){
                    response=getLoanAmountValSSB(request,"Amount must be numeric and not be greater than maximum loan amount (ZWL\"+maxLoanAmt+\")");
                }else if(amount>maxLoanAmt){
                    response=getLoanAmountValSSB(request,"Amount must not be greater than maximum loan amount (ZWL"+maxLoanAmt+")");
                }else{
                    response = confirmNameDetails3(request);
                }


            }else if (menu.equalsIgnoreCase(StringsEnum.regConfirmLoanDetailsCommit.toString())) {

                List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
                maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());
                double loanAmount=Double.parseDouble(loans.getLoanAmount().toString());
                if(loanAmount>maxLoanAmt){
                    response=getNetSalaryVal(request,"Re-enter Net Salary");
                }else if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {
                    response = confirmNameDetails2(request);
                }else if(message.equalsIgnoreCase(NumbersEnum.two.toString()) && loans.getLoanType().equalsIgnoreCase("Emergency Loan")) {
                    response = getLoanAmountEmergency(request);

                }else if(message.equalsIgnoreCase(NumbersEnum.two.toString()) && !(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))) {
                    response = getLoansMenu(request);

                }else if(message.equalsIgnoreCase(NumbersEnum.three.toString()) && loans.getLoanType().equalsIgnoreCase("Emergency Loan")) {
                    subscriberSession.setId(subscriberSession.getId());
                    subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                    subscriberSession.setMenu(StringsEnum.repayments.toString());
                    subscriberSessionRepository.save(subscriberSession);
                response = this.initialMenuService.getInitialMenu(request);

                    } else if(message.equalsIgnoreCase(NumbersEnum.three.toString()) && !(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))) {
                    subscriberSession.setId(subscriberSession.getId());
                    subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                    subscriberSession.setMenu(StringsEnum.repayments.toString());
                    subscriberSessionRepository.save(subscriberSession);
                response = this.initialMenuService.getInitialMenu(request);


                    }

            }else if (menu.equalsIgnoreCase(StringsEnum.regConfirmLoanDetailsFinal.toString())) {

                if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {
                    response = nameDetailsComplete(request);
                }else if(message.equalsIgnoreCase(NumbersEnum.two.toString()) && loans.getLoanType().equalsIgnoreCase("Emergency Loan")) {
                    response = getLoanAmountEmergency(request);

                }else if(message.equalsIgnoreCase(NumbersEnum.three.toString())) {
                    subscriberSession.setId(subscriberSession.getId());
                    subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                    subscriberSession.setMenu(StringsEnum.repayments.toString());
                    subscriberSessionRepository.save(subscriberSession);
                    response = this.initialMenuService.getInitialMenu(request);

                }else if(message.equalsIgnoreCase(NumbersEnum.two.toString()) && !(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))) {
                    response = getLoansMenu(request);

                } else {
                     response = genericService.invalidInput(request);
                }

            }
        }else {

            LOGGER.info("Invalid response is {}", request.getMessage());
            response = genericService.invalidInput(request);
        }


        return response;

    }

    @Override
    public MessageResponseType getLoansMenu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSessionRepository.save(subscriberSession);

        loans=new Loans();

        messageResponseType.setMessage(StringsEnum.loanMenu.toString());
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmount(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("Instant Loan");
        //get maximum loan amount
        List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
        maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());

        messageResponseType.setMessage("Please Enter Loan Amount: Maximum Loan Amount (ZWL"+maxLoanAmt+")");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmountEmergency(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("Emergency Loan");
        List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
        maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());
        messageResponseType.setMessage("Please Enter Loan Amount: Maximum Loan Amount (ZWL"+maxLoanAmt+")");

        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmount2(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("General Loan");
        List<CreditProducts> creditProductsList = creditProductsRepository.findByDisplayName(loans.getLoanType());
        maxLoanAmt=Double.parseDouble(creditProductsList.get(0).getMaxAmt().toString());
        messageResponseType.setMessage("Please Enter Loan Amount: Maximum Loan Amount (ZWL"+maxLoanAmt+")");

        return messageResponseType;
    }
    @Override
    public MessageResponseType getTenure(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.confirmCompleteLon.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

       loans.setLoanAmount(new BigDecimal(request.getMessage()));

        messageResponseType.setMessage("Please Enter Tenure(months):");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmountVal(MessageRequestType request,String message) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(message);
        return messageResponseType;
    }
    @Override
    public MessageResponseType getTenureVal(MessageRequestType request,String message) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.confirmCompleteLon.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(message);
        return messageResponseType;
    }
    @Override
    public MessageResponseType nameDetailsComplete(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.completeNameDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setNameDetails(true);
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        String message = StringsEnum.loanDetailsComplete.toString() + "Loan of RTGS"+loans.getLoanAmount()+" with a tenure of "+ loans.getTenure()+" months. "+genericService.getCurrentDate();


        loans.setLoanAmount(loans.getLoanAmount());
        loans.setTenure(loans.getTenure());
        loans.setEcno(registerSession.getEcNumber());
        loans.setMobile(registerSession.getPhone());
        loans.setIDNumber(registerSession.getNationalId());
        loans.setLoanDate(genericService.getCurrentDate());
        loans.setStage("SUBMITTED");
        loans.setSendTo("4041");
         loansRepository.save(loans);
        messageResponseType.setMessage("Loan successfully submitted.An SMS will follow shortly"+"\n"+ StringsEnum.contmenu.toString());
        this.genericService.SendSms(message,request.getSourceNumber());
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        return messageResponseType;
    }
    @Override
    public MessageResponseType confirmNameDetails(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmLoanDetailsCommit.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());


        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        loans.setTenure(new BigDecimal(request.getMessage()));
        if(loans.getInstallment()==null)
        {
            loans.setInstallment(new BigDecimal(0));
        }
        if(loans.getNetSalary()==null)
        {
            loans.setNetSalary(new BigDecimal(0.0));
        }

        String message = "Available Loan\n" +
                "Loan Amount: " + loans.getLoanAmount() + "\n" +
                "Loan Tenure: " + loans.getTenure() + "\n" +
                "Loan Type: " + loans.getLoanType() + "\n" +
                "1.Proceed , 2.Back ,3.Main Menu";

        if(loans.getNetSalary()!=null){
            message = "Available Loan\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed , 2.Back ,3.Main Menu";

        }
		  if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
 double loanamount=0.0;
        double admindrawdown=0.0;
        double totalloanamount=0.0;
        double interestrate=0.0;
       double tenure=0;
        double totalinterest=0.0;
        double maturityvalue=0.0;
        double installmentcommission=0.0;
        double monthlycommission=0;
        double totalinstallment=0.0;


        loanamount= Double.parseDouble(loans.getLoanAmount().toString());
        tenure= Double.parseDouble(loans.getTenure().toString());
        admindrawdown=loanamount*0.05;
         totalloanamount=loanamount+admindrawdown;
        interestrate=0.1;
        totalinterest=interestrate*tenure*totalloanamount;
        maturityvalue=totalinterest+totalloanamount;
       installmentcommission=maturityvalue/tenure;
        monthlycommission=0.025*installmentcommission;
        double finalinstallment=installmentcommission+monthlycommission;
		loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));
loans.setNetSalary(new BigDecimal(0.0));
              message = "Available Loan\n" +
                      "Loan Amount: " + loans.getLoanAmount() + "\n" +
                      "SSB Installment: " + loans.getInstallment() + "\n" +
                      "Loan Tenure: " + loans.getTenure() + "\n" +
                      "1.Proceed , 2.Back ,3.Main Menu";
		  }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
   double loanamount=0.0;
            double admindrawdown=0.0;
            double totalloanamount=0.0;
            double interestrate=0.0;
            double tenure=0;
            double totalinterest=0.0;
            double maturityvalue=0.0;
            double installmentcommission=0.0;
            double monthlycommission=0;
            double totalinstallment=0.0;


            loanamount= Double.parseDouble(loans.getLoanAmount().toString());
            tenure= Double.parseDouble(loans.getTenure().toString());
            admindrawdown=loanamount*0.05;
           totalloanamount=loanamount+admindrawdown;
            interestrate=0.1;
          totalinterest=interestrate*tenure*totalloanamount;
            maturityvalue=totalinterest+totalloanamount;
            installmentcommission=maturityvalue/tenure;
            monthlycommission=0.025*installmentcommission;
            double finalinstallment=installmentcommission+monthlycommission;
            loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));
            loans.setNetSalary(new BigDecimal(0.0));
            message = "Available Loan\n" +
                   "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +

                    "1.Proceed , 2.Back ,3.Main Menu";
        }

        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            double netsalary=Double.parseDouble(loans.getNetSalary().toString());
            double deductibleamount=netsalary-150;
            double tenure=Double.parseDouble(loans.getTenure().toString());
            double j16=1.05*0.025;
            double k16=1.05+j16;
            double c15=0.08;

            double finalamount=(deductibleamount*tenure)/((k16+(k16*c15*tenure)));


            finalamount=roundDown(finalamount,10);

            double amount=finalamount;
            double adminfee=0.05*amount;
            double loanamount=amount+adminfee;
            double rate=0.08;
            double totalinterest=loanamount*rate*tenure;
            double maturityvalue=loanamount+totalinterest;
            double installmentbeforecomission=maturityvalue/tenure;
            double monthlycommission=0.025*installmentbeforecomission;
            double finalinstallment=installmentbeforecomission+monthlycommission;
            finalinstallment=Math.round(finalinstallment * 100.0) / 100.0;
             loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));
            loans.setLoanAmount(new BigDecimal(finalamount).setScale(4, BigDecimal.ROUND_HALF_UP));
            message = "Available Loan\n" +
                    "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Maximum Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +

                    "1.Proceed , 2.Back ,3.Main Menu";

            if(finalamount<100) {
                message = "Available Loan\n" +
                       "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Maximum Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "2.Back ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }
            if(registerSession.getCustodian().equalsIgnoreCase("SSB")){
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setMenu(StringsEnum.regConfirmLoanAmount.toString());
                subscriberSession.setTransactionDate(genericService.getCurrentDate());
                subscriberSessionRepository.save(subscriberSession);
            }
            if(registerSession.getCustodian().equalsIgnoreCase("NEWSSB")){
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setMenu(StringsEnum.regConfirmLoanAmount.toString());
                subscriberSession.setTransactionDate(genericService.getCurrentDate());
                subscriberSessionRepository.save(subscriberSession);
            }
        }
        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            double netsalary=Double.parseDouble(loans.getNetSalary().toString());
            double deductibleamount=netsalary-150;
            double tenure=Double.parseDouble(loans.getTenure().toString());
            double j16=1.05*0.025;
            double k16=1.05+j16;
            double c15=0.08;

            double finalamount=(deductibleamount*tenure)/((k16+(k16*c15*tenure)));


            finalamount=roundDown(finalamount,10);

           double amount=finalamount;
            double adminfee=0.05*amount;
            double loanamount=amount+adminfee;
            double rate=0.08;
            double totalinterest=loanamount*rate*tenure;
            double maturityvalue=loanamount+totalinterest;
            double installmentbeforecomission=maturityvalue/tenure;
            double monthlycommission=0.025*installmentbeforecomission;
            double finalinstallment=installmentbeforecomission+monthlycommission;
            finalinstallment=Math.round(finalinstallment * 100.0) / 100.0;
            //System.out.println("SSB Installment "+ finalinstallment);
            loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));
            loans.setLoanAmount(new BigDecimal(finalamount).setScale(4, BigDecimal.ROUND_HALF_UP));
            message = "Available Loan\n" +
                   "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Maximum Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "1.Proceed , 2.Back ,3.Main Menu";
            if(finalamount<100) {
                message = "Available Loan\n" +
                       "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Maximum Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "2.Back ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }
            if(registerSession.getCustodian().equalsIgnoreCase("SSB")){
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setMenu(StringsEnum.regConfirmLoanAmount.toString());
                subscriberSession.setTransactionDate(genericService.getCurrentDate());
                subscriberSessionRepository.save(subscriberSession);
            }
            if(registerSession.getCustodian().equalsIgnoreCase("NEWSSB")){
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setMenu(StringsEnum.regConfirmLoanAmount.toString());
                subscriberSession.setTransactionDate(genericService.getCurrentDate());
                subscriberSessionRepository.save(subscriberSession);
            }

        }
            messageResponseType.setMessage(message);


        return messageResponseType;
    }
    @Override
    public MessageResponseType confirmNameDetails2(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmLoanDetailsFinal.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());

        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        String message = "Confirm Loan Details?\n" +
                "Names: " + registerSession.getFirstname() + " " +
                registerSession.getSurname() + "\n" +
                "Loan Amount: " + loans.getLoanAmount() + "\n" +
                "Loan Tenure: " + loans.getTenure() + "\n" +
                "Loan Type: " + loans.getLoanType() + "\n" +
                "1.Proceed 2.Cancel ,3.Main Menu";

        if(loans.getNetSalary()!=null ){
            message = "Confirm Loan Details?\n" +
                   "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";

        }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
   message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
        }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                     "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
        }

        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";

            if(Double.parseDouble(loans.getLoanAmount().toString())<100) {
                message = "Loan Details?\n" +
                        "Names: " + registerSession.getFirstname() + " " +
                        registerSession.getSurname() + "\n" +
                        "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Type: " + loans.getLoanType() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "1.Proceed 2.Cancel ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }


        }
        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                     "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
            if(Double.parseDouble(loans.getLoanAmount().toString())<100) {
                message = "Loan Details?\n" +
                        "Names: " + registerSession.getFirstname() + " " +
                        registerSession.getSurname() + "\n" +
                        "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Type: " + loans.getLoanType() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "1.Proceed 2.Cancel ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }

        }
        messageResponseType.setMessage(message);


        return messageResponseType;
    }
    @Override
    public MessageResponseType confirmNameDetails3(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmLoanDetailsFinal.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());

        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);
//set loan amount
        loans.setLoanAmount(new BigDecimal(request.getMessage()));
        String message = "Confirm Loan Details?\n" +
                "Names: " + registerSession.getFirstname() + " " +
                registerSession.getSurname() + "\n" +
                "Loan Amount: " + loans.getLoanAmount() + "\n" +
                "Loan Tenure: " + loans.getTenure() + "\n" +
                "Loan Type: " + loans.getLoanType() + "\n" +
                "1.Proceed 2.Cancel ,3.Main Menu";

        if(loans.getNetSalary()!=null ){
            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";

        }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
           try{
              getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

           }catch(Exception i){

           }

            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
        }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
        }

        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";

            if(Double.parseDouble(loans.getLoanAmount().toString())<100) {
                try{
                    getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

                }catch(Exception i){

                }
                message = "Loan Details?\n" +
                        "Names: " + registerSession.getFirstname() + " " +
                        registerSession.getSurname() + "\n" +
                        "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Type: " + loans.getLoanType() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "1.Proceed 2.Cancel ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }


        }
        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            try{
                getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

            }catch(Exception i){

            }
            message = "Confirm Loan Details?\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "Net Salary: " + loans.getNetSalary() + "\n" +
                    "Loan Amount: " + loans.getLoanAmount() + "\n" +
                    "SSB Installment: " + loans.getInstallment() + "\n" +
                    "Loan Tenure: " + loans.getTenure() + "\n" +
                    "Loan Type: " + loans.getLoanType() + "\n" +
                    "1.Proceed 2.Cancel ,3.Main Menu";
            if(Double.parseDouble(loans.getLoanAmount().toString())<100) {
                try{
                    getFinalInstallmentSSB(request,Double.parseDouble(loans.getLoanAmount().toString()));

                }catch(Exception i){

                }
                message = "Loan Details?\n" +
                        "Names: " + registerSession.getFirstname() + " " +
                        registerSession.getSurname() + "\n" +
                        "Net Salary: " + loans.getNetSalary() + "\n" +
                        "Loan Amount: " + loans.getLoanAmount() + "\n" +
                        "SSB Installment: " + loans.getInstallment() + "\n" +
                        "Loan Tenure: " + loans.getTenure() + "\n" +
                        "Loan Type: " + loans.getLoanType() + "\n" +
                        "Loan Status: Not Eligible \n" +
                        "1.Proceed 2.Cancel ,3.Main Menu";
                subscriberSession.setId(subscriberSession.getId());
                subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                subscriberSession.setMenu(StringsEnum.repayments.toString());
                subscriberSessionRepository.save(subscriberSession);
            }

        }
        messageResponseType.setMessage(message);


        return messageResponseType;
    }
    @Override
    public  int roundDown(double number, double place) {
        double result = number / place;
        result = Math.floor(result);
        result *= place;
        return (int)result;
    }
    @Override
    public MessageResponseType getLoanTracker(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");

        String message = StringsEnum.comingSoon.toString();

        List<Loans> loansList=loansRepository.findByMobile(request.getSourceNumber());
        StringBuffer buffer = new StringBuffer();

        if (loansList == null || loansList.isEmpty()) {

            buffer.append("You have not applied for any loans.");

        } else {

            int max = 5;

            if (loansList.size() <=5)
                max = loansList.size();

            for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {
                   buffer.append(loansList.get(index).getId() + ".ZWL" +
                        this.genericService.numberRounder(loansList.get(index).getLoanAmount(),2) + "." +loansList.get(index).getStage()+"\n");
            }

        }
        messageResponseType.setMessage("You will receive an Sms shortly \n"+ StringsEnum.contmenu.toString());
       System.out.println(buffer.toString());
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        return  messageResponseType;
    }
    @Override
    public MessageResponseType getNetSalary2(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure2.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("General Loan");

        messageResponseType.setMessage("Please Enter Net Salary");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getNetSalary(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure2.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("Instant Loan");
        //get maximum loan amount

        messageResponseType.setMessage("Please Enter Net Salary");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getTenure2(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.confirmCompleteLon.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
      loans.setLoanAmount(new BigDecimal(request.getMessage()));
        loans.setNetSalary(new BigDecimal(request.getMessage()));
        messageResponseType.setMessage("Please Enter Tenure(3-12 Months):");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getNetSalaryVal(MessageRequestType request,String message) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.loanSubMenu.toString());
        subscriberSession.setMenu(StringsEnum.regTenure2.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(message);
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmountSSB(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmLoanCommit.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);
        loans.setLoanType("Instant Loan");
        //get maximum loan amount
        maxLoanAmt=Double.parseDouble(loans.getLoanAmount().toString());

        messageResponseType.setMessage("Please Enter Loan Amount: Maximum Loan Amount (ZWL"+maxLoanAmt+")");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLoanAmountValSSB(MessageRequestType request,String message) {

        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmLoanCommit.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(message);
        return messageResponseType;
    }
    @Override
    public void getFinalInstallmentSSB(MessageRequestType request,double loanamounts){



        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            double loanamount=0.0;
            double admindrawdown=0.0;
            double totalloanamount=0.0;
            double interestrate=0.0;
            double tenure=0;
            double totalinterest=0.0;
            double maturityvalue=0.0;
            double installmentcommission=0.0;
            double monthlycommission=0;
            double totalinstallment=0.0;


            loanamount=loanamounts;
            tenure= Double.parseDouble(loans.getTenure().toString());
            admindrawdown=loanamount*0.05;
            totalloanamount=loanamount+admindrawdown;
            interestrate=0.1;
            totalinterest=interestrate*tenure*totalloanamount;
            maturityvalue=totalinterest+totalloanamount;
            installmentcommission=maturityvalue/tenure;
            monthlycommission=0.025*installmentcommission;
            double finalinstallment=installmentcommission+monthlycommission;
     }
        if(loans.getLoanType().equalsIgnoreCase("Emergency Loan")&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            double loanamount=0.0;
            double admindrawdown=0.0;
            double totalloanamount=0.0;
            double interestrate=0.0;
            double tenure=0;
            double totalinterest=0.0;
            double maturityvalue=0.0;
            double installmentcommission=0.0;
            double monthlycommission=0;
            double totalinstallment=0.0;


            loanamount=loanamounts;
            tenure= Double.parseDouble(loans.getTenure().toString());
            admindrawdown=loanamount*0.05;
            totalloanamount=loanamount+admindrawdown;
            interestrate=0.1;
            totalinterest=interestrate*tenure*totalloanamount;
            maturityvalue=totalinterest+totalloanamount;
            installmentcommission=maturityvalue/tenure;
            monthlycommission=0.025*installmentcommission;
            double finalinstallment=installmentcommission+monthlycommission;
            loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));

        }

        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("SSB") ){
            double netsalary=Double.parseDouble(loans.getNetSalary().toString());
            double deductibleamount=netsalary-150;
            double tenure=Double.parseDouble(loans.getTenure().toString());
            double j16=1.05*0.025;
            double k16=1.05+j16;
            double c15=0.08;

            double finalamount=(deductibleamount*tenure)/((k16+(k16*c15*tenure)));


            finalamount=roundDown(finalamount,10);

            double amount=loanamounts;
            double adminfee=0.05*amount;
            double loanamount=amount+adminfee;
            double rate=0.08;
            double totalinterest=loanamount*rate*tenure;
            double maturityvalue=loanamount+totalinterest;
            double installmentbeforecomission=maturityvalue/tenure;
            double monthlycommission=0.025*installmentbeforecomission;
            double finalinstallment=installmentbeforecomission+monthlycommission;
            finalinstallment=Math.round(finalinstallment * 100.0) / 100.0;
            loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));

        }
        if(!(loans.getLoanType().equalsIgnoreCase("Emergency Loan"))&& this.genericService.getSSB(request).equalsIgnoreCase("NEWSSB") ){
            double netsalary=Double.parseDouble(loans.getNetSalary().toString());
            double deductibleamount=netsalary-150;
            double tenure=Double.parseDouble(loans.getTenure().toString());
            double j16=1.05*0.025;
            double k16=1.05+j16;
            double c15=0.08;

            double finalamount=(deductibleamount*tenure)/((k16+(k16*c15*tenure)));


            finalamount=roundDown(finalamount,10);

            double amount=loanamounts;
            double adminfee=0.05*amount;
            double loanamount=amount+adminfee;
            double rate=0.08;
            double totalinterest=loanamount*rate*tenure;
            double maturityvalue=loanamount+totalinterest;
            double installmentbeforecomission=maturityvalue/tenure;
            double monthlycommission=0.025*installmentbeforecomission;
            double finalinstallment=installmentbeforecomission+monthlycommission;
            finalinstallment=Math.round(finalinstallment * 100.0) / 100.0;
            loans.setInstallment(new BigDecimal(finalinstallment).setScale(4, BigDecimal.ROUND_HALF_UP));



        }

    }
    @Override
    public MessageResponseType LoanAuthorization(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"LOANS");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.completeNameDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setNameDetails(true);
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);


        messageResponseType.setMessage("Your account has not been activated to apply for loans.Please contact the Untu Capital Limited for more information.\n"+StringsEnum.contmenu.toString());
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        return messageResponseType;
    }
}
