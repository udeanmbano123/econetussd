package zw.co.escrow.brokerussd.services;

import zw.co.escrow.brokerussd.pojos.*;
import zw.co.escrow.brokerussd.pojos.persistance.CompanySecurities;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.*;
import zw.co.escrow.brokerussd.pojos.register.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Udean Mbano
 */
public interface GenericService {

    MessageResponseType exitUssd(MessageRequestType request);
    MessageResponseType exitUssd(MessageRequestType request,String transactiontype);

    MessageResponseType invalidInput(MessageRequestType request);


    MessageResponseType getTerms(MessageRequestType request);

    MessageResponseType getReg(MessageRequestType request);

    MessageResponseType setGenericResponse(MessageRequestType request, MessageResponseType response);
    MessageResponseType setGenericResponse(MessageRequestType request, MessageResponseType response,String transactiontype);
    MessageResponseType setFinalResponse(MessageRequestType request, MessageResponseType response);
    MessageResponseType setFinalResponse(MessageRequestType request, MessageResponseType response,String transactiontype);
    Date getCurrentDate();

    SubscriberSession getSubscriberSession(MessageRequestType requestType);

    RegisterSession getRegisterSession(MessageRequestType request);

    TradeSession getTradeSession(MessageRequestType request);

     String isIdExists(MessageRequestType request, String id, String mobile);


     List<String> getMyTitles();

    List<String> getGender();

    List<String> getMyCountries();

    Map<String, String> allBanks();


    List<Bank> getBankList(MessageRequestType requestType);

    List<BankNext> getBanksNext(MessageRequestType requestType, String page);


    List<Branch> getBranchList(MessageRequestType requestType);

    Boolean inputIsNumber(String input);

    Boolean isDateValid(String date);

    Boolean inputIsDouble(String input);

    String getCdsNumber(MessageRequestType requestType);
    String getSSB(MessageRequestType requestType);



    String getApplicationId();

    Integer getSessionId();
    String convertDate(Date date,String format);
    BigDecimal numberRounder(BigDecimal bigDecimal,Integer decimalPlaces);
    boolean isNullOrEmpty(String str);
    boolean isNumeric(String str);
    boolean isECNumber(String input);
    boolean isExistingAccount(String input);
    boolean isRegisteredAccount(String input);
    public void SendSms(String message,String mobile);
    String decode(String url);
    String encode(String url);
    MessageResponseType SmsNotification(MessageRequestType request);
    public String isLoanBalance(String input);
    public String isLoanBalanceMambu(String input);
    boolean isAuthAccount(String input);
}
