package zw.co.escrow.brokerussd.services.myaccount;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface Myaccount {

    MessageResponseType myaccountDispatcherMenu(MessageRequestType messageRequestType);

    MessageResponseType getaccountMenu(MessageRequestType request);

    MessageResponseType getAccountStatus(MessageRequestType request);

    MessageResponseType getRepayments(MessageRequestType request);

    MessageResponseType getAccountBalance(MessageRequestType request);
}
