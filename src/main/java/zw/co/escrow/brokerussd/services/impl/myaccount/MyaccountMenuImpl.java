package zw.co.escrow.brokerussd.services.impl.myaccount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.escrow.brokerussd.constants.MultiDataTypesFormatter;
import zw.co.escrow.brokerussd.constants.NumbersEnum;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.subscriber.RegisterSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscribersRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.UssdMobileRepository;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.RegisterSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.Subscribers;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.UssdMobile;
import zw.co.escrow.brokerussd.pojos.register.Bank;
import zw.co.escrow.brokerussd.pojos.register.BankNext;
import zw.co.escrow.brokerussd.pojos.register.Custodian;
import zw.co.escrow.brokerussd.services.GenericService;
import zw.co.escrow.brokerussd.services.InitialMenuService;
import zw.co.escrow.brokerussd.services.loans.Loans;
import zw.co.escrow.brokerussd.services.myaccount.Myaccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Udean Mbano
 */
@Service
public class MyaccountMenuImpl implements Myaccount {

    private GenericService genericService;
    private InitialMenuService initialMenuService;
    private List<String> titles,
            gender,
            nationalities;
    private List<Custodian> custodians;
    private List<Bank> banks;
    private List<BankNext> banksNext;
    private RegisterSession registerSession;
    private SubscriberSession subscriberSession;
    private RegisterSessionRepository registerSessionRepository;
    private SubscriberSessionRepository subscriberSessionRepository;
    private SubscribersRepository subscribersRepository;
    private MessageResponseType messageResponseType;
    private UssdMobileRepository ussdMobileRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(MyaccountMenuImpl.class);

    @Autowired
    public MyaccountMenuImpl(InitialMenuService initialMenuService,GenericService genericService,
                             RegisterSessionRepository registerSessionRepository,
                             SubscriberSessionRepository subscriberSessionRepository,UssdMobileRepository ussdMobileRepository,SubscribersRepository subscribersRepository) {
        this.genericService = genericService;
        this.registerSessionRepository = registerSessionRepository;
        this.initialMenuService = initialMenuService;
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.messageResponseType = new MessageResponseType();
        this.titles = new ArrayList<>();
        this.nationalities = new ArrayList<>();
        this.ussdMobileRepository = ussdMobileRepository;
        this.gender = new ArrayList<>();
        this.custodians = new ArrayList<>();
        this.banks = new ArrayList<>();
        this.banksNext = new ArrayList<>();
        this.subscribersRepository=subscribersRepository;
    }


    @Override
    public MessageResponseType myaccountDispatcherMenu(MessageRequestType request) {

        MessageResponseType response = null;

        subscriberSession = genericService.getSubscriberSession(request);

        String message = request.getMessage();
        String menu = subscriberSession.getMenu();
        String rootMenu = subscriberSession.getRootMenu();
        response = getaccountMenu(request);

       if (menu.equalsIgnoreCase(StringsEnum.initial.toString())) {
            response = this.initialMenuService.getInitialMenu(request);
        }else if (menu.equalsIgnoreCase(StringsEnum.regMenuOne.toString())){
            response = getaccountMenu(request);

        }else if (rootMenu.equalsIgnoreCase(StringsEnum.AccSubMenu.toString())) {


            if (message.equalsIgnoreCase(NumbersEnum.one.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.AccSubMenu.toString()) ) {
                MultiDataTypesFormatter.noSpecialChars(request.getMessage());
                response = getRepayments(request);

            } else if (message.equalsIgnoreCase(NumbersEnum.two.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.AccSubMenu.toString())  ) {
                response = getAccountStatus(request);

            }
            else if (message.equalsIgnoreCase(NumbersEnum.three.toString()) &&
                    menu.equalsIgnoreCase(StringsEnum.AccSubMenu.toString())  ) {
                response = getAccountBalance(request);

            }
        }
        return response;

    }

    @Override
    public MessageResponseType getaccountMenu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"ACCOUNTSTATUS");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.AccSubMenu.toString());
        subscriberSession.setRootMenu(StringsEnum.AccSubMenu.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);

        registerSession.setId(registerSession.getId());


        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(StringsEnum.accountMenu.toString());
        return messageResponseType;
    }
    @Override
    public MessageResponseType getAccountStatus(MessageRequestType request) {
     //
        this.genericService.setGenericResponse(request, messageResponseType,"ACCOUNTSTATUS");
        registerSession.setId(registerSession.getId());

       registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        List<Subscribers> subscribersList=subscribersRepository.findByPhoneNumber(request.getSourceNumber());
        StringBuffer buffer = new StringBuffer();

        if (subscribersList == null || subscribersList.isEmpty()) {

            buffer.append("You have not applied for any loans.");

        } else {

            int max = 5;

            if (subscribersList.size() <=1)
                max = subscribersList.size();

            for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {
   buffer.append("Account Status Profile \n" +
                        "EC Number: " + registerSession.getEcNumber() + "\n" +
                        "Names: " + registerSession.getFirstname() + " " +
                        registerSession.getSurname() + "\n" +
                        "National Id: " + registerSession.getNationalId() + "\n" +
                        "Mobile Number: " + registerSession.getPhone() + "\n" +
                        "Date Registered: " + this.genericService.convertDate(subscribersList.get(index).getDate(),"dd-MMM-yyyy") + "\n" +
                        "");
            }

        }
        subscriberSession.setId(subscriberSession.getId());

        messageResponseType.setMessage("You will receive an sms shortly"+"\n"+ StringsEnum.contmenu2.toString());
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        this.genericService.SendSms(buffer.toString(),request.getSourceNumber());
        return messageResponseType;
    }
    @Override
    public MessageResponseType getAccountBalance(MessageRequestType request) {
        //
        this.genericService.setGenericResponse(request, messageResponseType,"ACCOUNTSTATUS");
        registerSession.setId(registerSession.getId());

        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        if(registerSession.getCustodian().equalsIgnoreCase("MAMBU")){
            messageResponseType.setMessage(this.genericService.isLoanBalanceMambu(registerSession.getEcNumber())+"\n"+ StringsEnum.contmenu2.toString());

        }else{
            messageResponseType.setMessage(this.genericService.isLoanBalance(registerSession.getEcNumber())+"\n"+ StringsEnum.contmenu2.toString());

        }
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        return messageResponseType;
    }
    @Override
    public MessageResponseType getRepayments(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"ACCOUNTSTATUS");
        subscriberSession.setId(subscriberSession.getId());
        messageResponseType.setMessage("Coming soon \n"+ StringsEnum.contmenu2.toString());
        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
        subscriberSession.setMenu(StringsEnum.repayments.toString());
        subscriberSessionRepository.save(subscriberSession);
        return messageResponseType;
    }
}
