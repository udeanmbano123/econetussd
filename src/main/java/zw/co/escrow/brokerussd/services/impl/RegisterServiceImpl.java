package zw.co.escrow.brokerussd.services.impl;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zw.co.escrow.brokerussd.constants.MultiDataTypesFormatter;
import zw.co.escrow.brokerussd.constants.NumbersEnum;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.ApiCredsR.ApiCredentialsRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.RegisterSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscribersRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.UssdMobileRepository;
import zw.co.escrow.brokerussd.pojos.ApiCreds.ApiCredentials;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;
import zw.co.escrow.brokerussd.pojos.loans.Loans;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.RegisterSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.Subscribers;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.UssdMobile;
import zw.co.escrow.brokerussd.pojos.register.Bank;
import zw.co.escrow.brokerussd.pojos.register.BankNext;
import zw.co.escrow.brokerussd.pojos.register.Custodian;
import zw.co.escrow.brokerussd.services.GenericService;
import zw.co.escrow.brokerussd.services.RegisterService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Udean Mbano
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    private GenericService genericService;
    private List<String> titles,
            gender,
            nationalities;
    private List<Custodian> custodians;
    private List<Bank> banks;
    private List<BankNext> banksNext;
    private RegisterSession registerSession;
    private SubscriberSession subscriberSession;
    private ApiCredentialsRepository apiCredentialsRepository;
    private RegisterSessionRepository registerSessionRepository;
    private SubscriberSessionRepository subscriberSessionRepository;
    private UssdMobileRepository ussdMobileRepository;
    private MessageResponseType messageResponseType;
    private SubscribersRepository subscribersRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterServiceImpl.class);

    @Autowired
    public RegisterServiceImpl(GenericService genericService,
                               RegisterSessionRepository registerSessionRepository,
                               SubscriberSessionRepository subscriberSessionRepository, SubscribersRepository subscribersRepository
            , UssdMobileRepository ussdMobileRepository, ApiCredentialsRepository apiCredentialsRepository) {
        this.genericService = genericService;
        this.registerSessionRepository = registerSessionRepository;
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.subscribersRepository = subscribersRepository;
        this.ussdMobileRepository = ussdMobileRepository;
        this.messageResponseType = new MessageResponseType();
        this.titles = new ArrayList<>();
        this.nationalities = new ArrayList<>();
        this.gender = new ArrayList<>();
        this.custodians = new ArrayList<>();
        this.banks = new ArrayList<>();
        this.banksNext = new ArrayList<>();
        this.apiCredentialsRepository = apiCredentialsRepository;
    }

    @Override
    public MessageResponseType registerDispatcherMenu(MessageRequestType request) {

        MessageResponseType response = null;

        subscriberSession = genericService.getSubscriberSession(request);

        String message = request.getMessage();
        String menu = subscriberSession.getMenu();
        String rootMenu = subscriberSession.getRootMenu();

        if (menu.equalsIgnoreCase(StringsEnum.unRegOne.toString())) {
            response = getRegisterMenu(request);
        } else if (menu.equalsIgnoreCase(StringsEnum.regSubMenu.toString()) &&
                rootMenu.equalsIgnoreCase(StringsEnum.regSubMenu.toString())) {

            registerSession = genericService.getRegisterSession(request);

            if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {
                MultiDataTypesFormatter.noSpecialChars(request.getMessage());
                response = getClientMenu(request);
            } else if (message.equalsIgnoreCase(NumbersEnum.two.toString())) {
                response = getLinkMenu(request);
            } else {
                response = genericService.invalidInput(request);
            }


        } else if (rootMenu.equalsIgnoreCase(StringsEnum.regNameDetails.toString())) {


            System.out.println("Menu value "+menu.toString());
            System.out.println("Request value "+request.getMessage());
            if (menu.equalsIgnoreCase(StringsEnum.regConfirmNew.toString()) && request.getMessage().equalsIgnoreCase(NumbersEnum.one.toString())) {

                response = getSurnameOrdinary(request);


            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmNew.toString()) && request.getMessage().equalsIgnoreCase(NumbersEnum.two.toString())) {

                response = getECNumSSB(request);

            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmlink.toString()) && request.getMessage().equalsIgnoreCase(NumbersEnum.one.toString())) {

                response = getECNumm(request);


            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmlink.toString()) && request.getMessage().equalsIgnoreCase(NumbersEnum.two.toString())) {

                response = getMambu(request);

            } else if (menu.equalsIgnoreCase(StringsEnum.regSurname.toString())) {
                if (request.getMessage().length() < 8) {
                   response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (request.getMessage().length() > 8) {
                  response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (this.genericService.isECNumber(request.getMessage()) == false) {
                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");

                } else if (isRegisteredAccount(request.getMessage()) == true) {
                    response=getECNumVal(request,"Account is already registered");

                } else {
                    response = getSurname(request);
                       }
            } else if (menu.equalsIgnoreCase(StringsEnum.regName.toString())) {

                LOGGER.info("in surname {}, {}", request.getMessage(),
                        "" + genericService.isNullOrEmpty(request.getMessage()));

                if (this.genericService.isNullOrEmpty(request.getMessage())) {

                    // doSomething
                    response = getSurname(request);
                } else {

                    response = getName(request);
                }

            } else if (menu.equalsIgnoreCase(StringsEnum.regNatId.toString())) {
                if (this.genericService.isNullOrEmpty(request.getMessage())) {
                    // doSomething
                    response = getName(request);
                } else {

                    response = getNationalId(request);
                }


            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmPass.toString())) {
                if (this.genericService.isNullOrEmpty(request.getMessage())) {
                    // doSomething
                    response = getNationalId(request);
                } else {

                    response = getPin(request);
                }

            }else if (menu.equalsIgnoreCase(StringsEnum.regecPass.toString())) {


                if (request.getMessage().length() < 8) {

                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (request.getMessage().length() > 8) {
                   response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (this.genericService.isECNumber(request.getMessage()) == false) {
                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");

                } else if (isRegisteredAccount(request.getMessage()) == true) {
                    response=getECNumVal(request,"Account is already registered");

                } else {

                    response = getPin2(request);
                }

            }else if (menu.equalsIgnoreCase(StringsEnum.regMambuPass.toString())) {


                if (request.getMessage().length() < 8) {

                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (request.getMessage().length() > 8) {
                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");
                } else if (this.genericService.isECNumber(request.getMessage()) == false) {
                    response=getECNumVal(request,"EC Number must have 7 digits and 1 character:(e.g 0000000A)");

                } else if (isRegisteredAccount(request.getMessage()) == true) {
                    response=getECNumVal(request,"Account is already registered");

                } else {

                    response = getMambu(request);
                }

            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmPass2.toString())) {
                if (this.genericService.isNullOrEmpty(request.getMessage())) {
                    // doSomething
                    response = getMambu(request);
                } else{
                    response = getPin2(request);
                }


            } else if (menu.equalsIgnoreCase(StringsEnum.regNewPin.toString())) {
                if (request.getMessage() == null && request.getMessage().isEmpty()) {
                    // doSomething
                    response = getPinVal(request, "Please enter your pin");
                } else if (request.getMessage().length() < 4) {
                    // doSomething
                    response = getPinVal(request, "Pin must be at least four digits");
                } else {

                    response = getConfirmPin(request);
                }
            } else if (menu.equalsIgnoreCase(StringsEnum.regGender.toString())) {
                response = confirmNameDetails(request);

            } else if (menu.equalsIgnoreCase(StringsEnum.regConfirmNameDetails.toString())) {

                String mno= registerSessionRepository.findByphone(request.getSourceNumber()).getCustodian();
                String ecnumber= registerSessionRepository.findByphone(request.getSourceNumber()).getEcNumber();
                String mobile= registerSessionRepository.findByphone(request.getSourceNumber()).getPhone();

             if (isExistingAccount(ecnumber) == true && mno.equalsIgnoreCase("NEW")) {

                    this.genericService.SendSms("Please select Link existing Account option,registration was not successful.",mobile);
                    response=this.genericService.SmsNotification(request);
                }else {
                    if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {
                        response = nameDetailsComplete(request);
                    } else {
                        response = genericService.invalidInput(request);
                    }
                }

            } else if (menu.equalsIgnoreCase(StringsEnum.completeNameDetails.toString())) {
                response = switchToMainMenu(request, message);
            } else {
                response = genericService.invalidInput(request);
            }

        } else if (rootMenu.equalsIgnoreCase(StringsEnum.existsAlready.toString())) {

            response = getRegisterMenu(request);

        } else {
            LOGGER.info("Invalid response is {}", request.getMessage());
            response = genericService.invalidInput(request);
        }

        return response;

    }

    @Override
    public MessageResponseType getRegisterMenu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType);
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regSubMenu.toString());
        subscriberSession.setRootMenu(StringsEnum.regSubMenu.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);

        if (registerSession == null) {
            registerSession = new RegisterSession();
            registerSession.setPhone(request.getSourceNumber());
            registerSession.setPersonalDetails(false);
            registerSession.setCustodianMenu(false);
            registerSession.setBankDetails(false);
            registerSession.setAddresses(false);
            registerSession.setAccount(false);
            registerSession.setNameDetails(false);
        } else {
            registerSession.setId(registerSession.getId());
        }

        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(StringsEnum.regMenu.toString());
        return messageResponseType;
    }

    @Override
    public MessageResponseType getTitle(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType);
        subscriberSession = genericService.getSubscriberSession(request);

        titles.clear();
        titles = genericService.getMyTitles();

        if (Integer.valueOf(request.getMessage()) > titles.size()) {

            return genericService.invalidInput(request);

        } else {

            StringBuffer buffer = new StringBuffer();

            buffer.append("Please Select Your Title:\n");

            for (int itemNumber = 1, index = 0; index < titles.size(); index++, itemNumber++) {
                buffer.append(itemNumber + ". " + titles.get(index) + "\n");
            }

            messageResponseType.setMessage(buffer.toString());
            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());

            subscriberSession.setMenu(StringsEnum.regTitle.toString());
            subscriberSession.setTransactionDate(genericService.getCurrentDate());
            subscriberSessionRepository.save(subscriberSession);

            return messageResponseType;
        }
    }

    @Override
    public MessageResponseType getClientMenu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType);
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regConfirmNew.toString());
        subscriberSessionRepository.save(subscriberSession);

//
       messageResponseType.setMessage(StringsEnum.regMenuClient.toString());
        return messageResponseType;
    }
    @Override
    public MessageResponseType getLinkMenu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType);
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regConfirmlink.toString());
        subscriberSessionRepository.save(subscriberSession);

//
        messageResponseType.setMessage(StringsEnum.regMenuClientLink.toString());
        return messageResponseType;
    }
    @Override
    public MessageResponseType getECNum(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType);
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regSurname.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setCustodian("NEW");
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter EC Number:(e.g 0000000A)");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getSurnameOrdinary(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regName.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setEcnumber("NONE");
        registerSession.setCustodian("NEWORDINARY");
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your Surname:");
        return messageResponseType;
    }
    @Override
    public MessageResponseType getECNumSSB(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regSurname.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setCustodian("NEWSSB");
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter EC Number:(e.g 0000000A)");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getECNumm(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regecPass.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setCustodian("SSB");
        registerSession.setCdsNumber("0");
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your EC Number:(e.g 0000000A)");

        return messageResponseType;
    }
    @Override
    public MessageResponseType getMambu(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setRootMenu(StringsEnum.regNameDetails.toString());
        subscriberSession.setMenu(StringsEnum.regConfirmPass2.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setEcnumber("None");
        registerSession.setCustodian("MAMBU");
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your Mambu Account Number:(e.g 0000000A)");

        return messageResponseType;
    }

    @Override
    public MessageResponseType getSurname(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regName.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setEcnumber(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your Surname:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getName(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regNatId.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setSurname(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your FirstName:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getMobile(MessageRequestType request) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regGender.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setNationalId(request.getMessage());

        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Mobile Number:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType confirmNameDetails(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);


        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmNameDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());

        //registerSession.setGender(gender.get(Integer.valueOf(request.getMessage()) - 1));
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        if(registerSession.getCustodian().equalsIgnoreCase("SSB")){
             boolean chk=isExistingAccount(registerSession.getEcNumber());
        }
        if(registerSession.getCustodian().equalsIgnoreCase("NEWSSB")){
            boolean chk=isExistingAccount(registerSession.getEcNumber());
        }

        if(registerSession.getCustodian().equalsIgnoreCase("MAMBU")){
            String  mambukey=isExistingAccountMambuLoan(registerSession.getEcNumber());
            System.out.println("Mambu key"+mambukey);
            boolean chk=isExistingAccountMambu(mambukey);
        }
        String message = "Confirm Name Details:\n" +
                "Names: " + registerSession.getFirstname() + " " +
                registerSession.getSurname() + "\n" +
                "National Id: " + registerSession.getNationalId() + "\n" +
                "Mobile Number: " + registerSession.getPhone() + "\n" +
                "1.Proceed";

        if(registerSession.getCustodian().equalsIgnoreCase("SSB")){
            message = "Confirm Name Details:\n" +
                    "EC Number: " + registerSession.getEcNumber() + "\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "National Id: " + registerSession.getNationalId() + "\n" +
                    "Mobile Number: " + registerSession.getPhone() + "\n" +
                    "1.Proceed";
        }
        if(registerSession.getCustodian().equalsIgnoreCase("NEWSSB")){
            message = "Confirm Name Details:\n" +
                    "EC Number: " + registerSession.getEcNumber() + "\n" +
                    "Names: " + registerSession.getFirstname() + " " +
                    registerSession.getSurname() + "\n" +
                    "National Id: " + registerSession.getNationalId() + "\n" +
                    "Mobile Number: " + registerSession.getPhone() + "\n" +
                    "1.Proceed";
        }

        messageResponseType.setMessage(message);


        return messageResponseType;
    }

    @Override
    public MessageResponseType confirmMenuTwoDetails(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmMenuTwoDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setEmail(request.getMessage());
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        String message = "Confirm Details:\n" +
                "Bank: " + registerSession.getBank() + "\n" +
                "Custodian: " + registerSession.getCustodian() + "\n" +
                "Account Number: " + registerSession.getAccountNo() + "\n" +
                "Physical Address: " + registerSession.getAddress() + "\n" +
                "Email: " + registerSession.getEmail() + "\n" +
                "1.Proceed";

        messageResponseType.setMessage(message);

        return messageResponseType;
    }

    @Override
    public MessageResponseType getNationalId(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmPass.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setFirstname(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your National Id:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getNationality(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        nationalities.clear();
        nationalities = genericService.getMyCountries();

        StringBuffer buffer = new StringBuffer();

        buffer.append("Please Select Your Nationality:\n");

        for (int itemNumber = 1, index = 0; index < nationalities.size(); index++, itemNumber++) {
            buffer.append(itemNumber + ". " + nationalities.get(index) + "\n");
        }

        messageResponseType.setMessage(buffer.toString());
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regNationality.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        //registerSession.setNationalId(request.getMessage());
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        return messageResponseType;
    }

    @Override
    public MessageResponseType getDob(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        if (Integer.valueOf(request.getMessage()) > nationalities.size()) {

            messageResponseType = genericService.invalidInput(request);

        } else {
            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.regDob.toString());
            subscriberSession.setTransactionDate(genericService.getCurrentDate());
            subscriberSessionRepository.save(subscriberSession);

            registerSession = genericService.getRegisterSession(request);
            registerSession.setId(registerSession.getId());
            registerSession.setNationality(nationalities.get(Integer.valueOf(request.getMessage()) - 1));
            registerSession.setTransactionDate(genericService.getCurrentDate());
            registerSessionRepository.save(registerSession);
            String message = "Please Enter Your Date Of Birth:\n" +
                    "Format (YYYY-MM-DD)";
            messageResponseType.setMessage(message);
        }

        return messageResponseType;
    }

    @Override
    public MessageResponseType getAddress(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        if (Integer.valueOf(request.getMessage()) > custodians.size()) {

            messageResponseType = genericService.invalidInput(request);

        } else {

            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.regAddress.toString());
            subscriberSession.setRootMenu(StringsEnum.regAddresses.toString());
            subscriberSessionRepository.save(subscriberSession);

            registerSession = genericService.getRegisterSession(request);
            registerSession.setId(registerSession.getId());
            registerSession.setCustodian(custodians.get(Integer.valueOf(request.getMessage()) - 1).getCode());
            registerSession.setTransactionDate(genericService.getCurrentDate());
            registerSessionRepository.save(registerSession);

            messageResponseType.setMessage("Please Enter Your Physical Address:");

        }


        return messageResponseType;
    }

    @Override
    public MessageResponseType getGender(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        gender.clear();
        gender = genericService.getGender();

        StringBuffer buffer = new StringBuffer();

        buffer.append("Please Select Your Gender:\n");

        for (int itemNumber = 1, index = 0; index < gender.size(); index++, itemNumber++) {
            buffer.append(itemNumber + ". " + gender.get(index) + "\n");
        }

        messageResponseType.setMessage(buffer.toString());
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regGender.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setDob(request.getMessage());
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        return messageResponseType;
    }



    @Override
    public MessageResponseType getBank(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regBank.toString());
        subscriberSession.setRootMenu(StringsEnum.regBankDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        banks.clear();
        banks = genericService.getBankList(request);

        StringBuffer buffer = new StringBuffer();

        buffer.append("Please Select Your Bank:\n");

        for (int itemNumber = 1, index = 0; index < 8; index++, itemNumber++) {
            buffer.append(itemNumber + ". " + banks.get(index).getName() + "\n");
        }

        messageResponseType.setMessage(buffer.toString());

        return messageResponseType;
    }

    @Override
    public MessageResponseType getBanksNext(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        registerSession = genericService.getRegisterSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regBank.toString());
        subscriberSession.setRootMenu(StringsEnum.regBankDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setTransactionDate(genericService.getCurrentDate());
        registerSessionRepository.save(registerSession);

        Integer currentPage = Integer.valueOf(registerSession.getCurrentPage());

        banksNext.clear();

        StringBuffer buffer = new StringBuffer();

        buffer.append("Please Select Your Bank:\n");

        String nextOrPrevious = "";

        int message = Integer.valueOf(request.getMessage());

        if (message == Integer.valueOf(NumbersEnum.seven.toString())) {

            currentPage += Integer.valueOf(NumbersEnum.one.toString());
            registerSession.setCurrentPage(currentPage);

        } else if (message == Integer.valueOf(NumbersEnum.zero.toString()) &
                !currentPage.equals(0)) {

            currentPage -= Integer.valueOf(NumbersEnum.one.toString());
            registerSession.setCurrentPage(currentPage);

        } else if (currentPage == Integer.valueOf(NumbersEnum.four.toString())) {

            registerSession.setCurrentPage(Integer.valueOf(NumbersEnum.zero.toString()));
        }

        if (currentPage < Integer.valueOf(NumbersEnum.four.toString())
                & currentPage >= Integer.valueOf(NumbersEnum.one.toString())) {

            registerSession.setOnBanks(true);

            nextOrPrevious = "7.Next Page\n0.Previous Page";

        } else if (currentPage.equals(Integer.valueOf(NumbersEnum.zero.toString()))) {

            if (message == Integer.valueOf(NumbersEnum.zero.toString())) {
                registerSession.setCurrentPage(Integer.
                        valueOf(NumbersEnum.four.toString()));
            }

            nextOrPrevious = "7.Next Page";

        } else if (currentPage.equals(Integer.valueOf(NumbersEnum.four.toString()))) {

            if (message == Integer.valueOf(NumbersEnum.seven.toString())) {
                registerSession.setCurrentPage(Integer.
                        valueOf(NumbersEnum.zero.toString()));
            }
            nextOrPrevious = "0.Previous Page";

        }

        LOGGER.info("Current page {}", currentPage);

        registerSessionRepository.save(registerSession);

        banksNext = genericService.
                getBanksNext(request, String.valueOf(registerSession.getCurrentPage()));

        int max = Integer.valueOf(NumbersEnum.six.toString());

        if (banksNext.size() < max)
            max = banksNext.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {
            buffer.append(itemNumber + ". " + banksNext.get(index).getBankName() + "\n");
        }

        buffer.append(nextOrPrevious);

        messageResponseType.setMessage(buffer.toString());

        return messageResponseType;
    }

    @Override
    public MessageResponseType getBranch(MessageRequestType request) {
        return null;
    }

    @Override
    public MessageResponseType getAccountNo(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        if (Integer.valueOf(request.getMessage()) > banksNext.size()) {

            messageResponseType = genericService.invalidInput(request);

        } else {

            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.regAccountNo.toString());
            subscriberSession.setTransactionDate(genericService.getCurrentDate());
            subscriberSessionRepository.save(subscriberSession);

            registerSession = genericService.getRegisterSession(request);
            registerSession.setId(registerSession.getId());
            registerSession.setOnBanks(false);
            registerSession.setBank(banksNext.get(Integer.valueOf(request.getMessage()) - 1).getBank());
            registerSession.setBranch(StringsEnum.defaultBranch.toString());
            registerSession.setTransactionDate(genericService.getCurrentDate());
            registerSessionRepository.save(registerSession);

            String message = "Please Enter Your Account Number:\n";
            messageResponseType.setMessage(message);
        }
        return messageResponseType;
    }

    @Override
    public MessageResponseType getEmail(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regEmail.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setAddress(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Enter Your Email Address:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getPassword(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regPass.toString());
        subscriberSession.setRootMenu(StringsEnum.regAccount.toString());
        subscriberSessionRepository.save(subscriberSession);

        messageResponseType.setMessage("Please Enter Your Password:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getConfirmPassword(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regConfirmPass.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setPassword(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Confirm Your Password:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getPin(MessageRequestType request) {
        registerSession = genericService.getRegisterSession(request);

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regNewPin.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession.setId(registerSession.getId());
        registerSession.setNationalId(request.getMessage());
        registerSessionRepository.save(registerSession);

        String message = "Please Enter Your Pin\n";
        messageResponseType.setMessage(message);

        return messageResponseType;
    }

    @Override
    public MessageResponseType getPin2(MessageRequestType request) {
        registerSession = genericService.getRegisterSession(request);

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regNewPin.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession.setId(registerSession.getId());
        registerSession.setCdsNumber(request.getMessage());
        registerSession.setEcnumber(request.getMessage());
        registerSessionRepository.save(registerSession);

        String message = "Please Enter Your Pin\n";
        messageResponseType.setMessage(message);

        return messageResponseType;
    }

    @Override
    public MessageResponseType getPinVal(MessageRequestType request, String message) {
        registerSession = genericService.getRegisterSession(request);

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regNewPin.toString());
        subscriberSessionRepository.save(subscriberSession);
        ;
        messageResponseType.setMessage(message);

        return messageResponseType;
    }

    @Override
    public MessageResponseType getConfirmPin(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.regGender.toString());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setPin(request.getMessage());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage("Please Confirm Your Pin:");
        return messageResponseType;
    }

    @Override
    public MessageResponseType getRegisterConfirm(MessageRequestType request) {
        registerSession = genericService.getRegisterSession(request);

        if (registerSession.getPin().equalsIgnoreCase(request.getMessage())) {
            this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
            subscriberSession = genericService.getSubscriberSession(request);
            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.confirmCompleteReg.toString());
            subscriberSessionRepository.save(subscriberSession);

            registerSession.setId(registerSession.getId());
            registerSessionRepository.save(registerSession);

            String message = "Proceed opening a C-Trade Account?\n" +
                    "1. Yes\n" +
                    "2. No";
            messageResponseType.setMessage(message);
        } else {
            genericService.setFinalResponse(request, messageResponseType);
            messageResponseType.setMessage(StringsEnum.pinsNotMatch.toString());
        }

        return messageResponseType;
    }

    @Override
    public MessageResponseType getRegisterComplete(MessageRequestType request) {

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setAccount(true);
        registerSessionRepository.save(registerSession);
        messageResponseType = saveUserToDatabase(request);
        return messageResponseType;
    }

    @Override
    public MessageResponseType personalDetailsComplete(MessageRequestType request) {
        genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        if (Integer.valueOf(request.getMessage()) > gender.size()) {

            messageResponseType = genericService.invalidInput(request);

        } else {

            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.completePersonalDetails.toString());
            subscriberSession.setTransactionDate(genericService.getCurrentDate());
            subscriberSessionRepository.save(subscriberSession);

            registerSession = genericService.getRegisterSession(request);
            registerSession.setId(registerSession.getId());
            registerSession.setGender(gender.get(Integer.valueOf(request.getMessage()) - 1));
            registerSession.setTransactionDate(genericService.getCurrentDate());
            registerSession.setPersonalDetails(true);
            registerSessionRepository.save(registerSession);

            String message = StringsEnum.personalDetails.toString()
                    .concat(StringsEnum.finishedRegistered.toString())
                    .concat(StringsEnum.addresses.toString())
                    .concat(StringsEnum.regCompleteChoices.toString());

            messageResponseType.setMessage(message);


        }

        return messageResponseType;
    }

    @Override
    public MessageResponseType custodianRegisterComplete(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        if (Integer.valueOf(request.getMessage()) > custodians.size()) {

            messageResponseType = genericService.invalidInput(request);

        } else {

            subscriberSession.setId(subscriberSession.getId());
            subscriberSession.setMenu(StringsEnum.regCompleteCustodian.toString());
            subscriberSession.setTransactionDate(genericService.getCurrentDate());
            subscriberSessionRepository.save(subscriberSession);

            registerSession = genericService.getRegisterSession(request);
            registerSession.setId(registerSession.getId());
            registerSession.setCustodianMenu(true);
            registerSession.setCustodian(custodians.get(Integer.valueOf(request.getMessage()) - 1).getCode());
            registerSession.setTransactionDate(genericService.getCurrentDate());
            registerSessionRepository.save(registerSession);

            String message = StringsEnum.custodianDetails.toString()
                    .concat(StringsEnum.finishedRegistered.toString())
                    .concat(StringsEnum.completeProcess.toString())
                    .concat(StringsEnum.regCompleteChoices.toString());

            messageResponseType.setMessage(message);

        }

        return messageResponseType;
    }

    @Override
    public MessageResponseType nameDetailsComplete(MessageRequestType request) {
        this.genericService.setFinalResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.completeNameDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setNameDetails(true);
        registerSession.setPersonalDetails(true);
        registerSessionRepository.save(registerSession);

        String message = StringsEnum.nameDetailsComplete.toString();

        Subscribers subscribers = new Subscribers();
        subscribers.setCdsNumber(registerSession.getEcNumber());
        subscribers.setEmail(registerSession.getNationalId());
        subscribers.setPhoneNumber(registerSession.getPhone());
        subscribers.setPin(registerSession.getPin());
        subscribers.setDate(genericService.getCurrentDate());
        subscribers.setUsername(registerSession.getNationalId());
        subscribers.setActive(true);
        subscribersRepository.save(subscribers);

        UssdMobile ussdMobile = new UssdMobile();
        ussdMobile.setCDSNumber(registerSession.getEcNumber());
        ussdMobile.setPin(registerSession.getPin());
        ussdMobile.setMobile(registerSession.getPhone());
        ussdMobile.setTransactionDate(genericService.getCurrentDate());
        ussdMobile.setSessionComplete(true);
        ussdMobileRepository.save(ussdMobile);

        messageResponseType.setMessage(message);
        this.genericService.SendSms(message,registerSession.getPhone());
        messageResponseType.setTransactionType("REGISTRATION");
        return messageResponseType;
    }

    @Override
    public MessageResponseType bankDetailsComplete(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.completeBankDetails.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setBankDetails(true);
        registerSession.setCustodianMenu(true);
        registerSession.setAddresses(true);
        registerSessionRepository.save(registerSession);

        String message = StringsEnum.bankDetailsComplete.toString();

        messageResponseType.setMessage(message);
        return messageResponseType;
    }

    @Override
    public MessageResponseType addressesComplete(MessageRequestType request) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.completeAddresses.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSession.setEmail(request.getMessage());
        registerSession.setAddresses(true);
        registerSessionRepository.save(registerSession);

        String message = StringsEnum.addresses.toString()
                .concat(StringsEnum.finishedRegistered.toString())
                .concat(StringsEnum.bankDetails.toString())
                .concat(StringsEnum.regCompleteChoices.toString());

        messageResponseType.setMessage(message);
        return messageResponseType;
    }

    @Override
    public MessageResponseType menuAlreadyRegistered(MessageRequestType request, String menu) {
        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);

        subscriberSession.setId(subscriberSession.getId());
        subscriberSession.setMenu(StringsEnum.existsAlready.toString());
        subscriberSession.setRootMenu(StringsEnum.existsAlready.toString());
        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);

        String message = menu.concat(StringsEnum.alreadyRegistered.toString())
                .concat(StringsEnum.regCompleteChoices.toString());

        messageResponseType.setMessage(message);
        return messageResponseType;
    }

    @Override
    public MessageResponseType alreadyRegistered(MessageRequestType request) {
        genericService.setFinalResponse(request, messageResponseType,"REGISTRATION");
        messageResponseType.setMessage(StringsEnum.inTheSystem.toString());
        return messageResponseType;
    }

    @Override
    public MessageResponseType saveUserToDatabase(MessageRequestType request) {
        genericService.setFinalResponse(request, messageResponseType,"REGISTRATION");

        if (getCreateAccountResult(request).contains(StringsEnum.accountSuccess.toString())) {
            messageResponseType.setMessage(StringsEnum.regComplete.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.emailExistsError.toString())) {
            messageResponseType.setMessage(StringsEnum.emailAlreadyExists.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.idExistsError.toString())) {
            messageResponseType.setMessage(StringsEnum.idAlreadyExists.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.missingFieldsError.toString())) {
            messageResponseType.setMessage(StringsEnum.missingFieldsError.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.missingIdNoError.toString())) {
            messageResponseType.setMessage(StringsEnum.missingIdNoError.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.invalidBrokerError.toString())) {
            messageResponseType.setMessage(StringsEnum.invalidBrokerError.toString());
        } else if (getCreateAccountResult(request).contains(StringsEnum.errorCreatingAccount.toString())) {
            messageResponseType.setMessage(StringsEnum.errorCreatingAccount.toString());
        } else {
            messageResponseType.setMessage(StringsEnum.registrationError.toString());
        }
        return messageResponseType;
    }

    private String getCreateAccountResult(MessageRequestType requestType) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        registerSession = genericService.getRegisterSession(requestType);

        final String url = StringsEnum.regUrl.toString();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("theTitle", registerSession.getTitle());
        map.add("theSurname", registerSession.getSurname());
        map.add("theName", registerSession.getFirstname());
        map.add("thenationality", registerSession.getNationality());
        map.add("theDOB", registerSession.getDob());
        map.add("theGender", registerSession.getGender());
        map.add("theCountry", registerSession.getNationality());
        map.add("idNums", registerSession.getNationalId());
        map.add("bank", registerSession.getBank());

        map.add("accountNum", registerSession.getAccountNo());
        map.add("email", registerSession.getEmail());
        map.add("adress_yangu", registerSession.getAddress());
        map.add("mobile", registerSession.getPhone());
        map.add("branch", StringsEnum.defaultBranch.toString());
        map.add("custodian", registerSession.getCustodian());
        map.add("password", registerSession.getPassword());
        map.add("pin", registerSession.getPin());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

        LOGGER.info("New account result {}", response);

        return response.toString();
    }

    public MessageResponseType lastPriority(MessageRequestType request) {
        genericService.setFinalResponse(request, messageResponseType,"REGISTRATION");
        messageResponseType.setMessage(StringsEnum.registerOthersFirst.toString());
        return messageResponseType;
    }

    public MessageResponseType registrationCancelled(MessageRequestType request) {
        genericService.setFinalResponse(request, messageResponseType,"REGISTRATION");
        messageResponseType.setMessage(StringsEnum.registrationCancelled.toString());
        return messageResponseType;
    }

    public MessageResponseType switchToMainMenu(MessageRequestType request, String message) {
        MessageResponseType response = null;

        String messsage = "Registration successful\n" +
                "EC Number: " + registerSession.getEcNumber() + "\n" +
                "Names: " + registerSession.getFirstname() + " " +
                registerSession.getSurname() + "\n" +
                "National Id: " + registerSession.getNationalId() + "\n" +
                "Mobile Number: " + registerSession.getPhone() + "\n";

        if (message.equalsIgnoreCase(NumbersEnum.one.toString())) {

            String menu = subscriberSession.getMenu();

            if (menu.equalsIgnoreCase(StringsEnum.completeNameDetails.toString())) {
                messageResponseType.setMessage(messsage.toString());
                messageResponseType.setTransactionType("REGISTRATION");
                return messageResponseType;
            } else if (menu.equalsIgnoreCase(StringsEnum.completeBankDetails.toString())) {
                response = getPassword(request);
            }
        } else if (message.equalsIgnoreCase(NumbersEnum.two.toString())) {
            response = genericService.exitUssd(request,"REGISTRATION");
        }
        return response;
    }

    @Override
    public MessageResponseType getECNumVal(MessageRequestType request, String validation) {

        this.genericService.setGenericResponse(request, messageResponseType,"REGISTRATION");
        subscriberSession = genericService.getSubscriberSession(request);
        subscriberSession.setId(subscriberSession.getId());
        subscriberSessionRepository.save(subscriberSession);

        registerSession = genericService.getRegisterSession(request);
        registerSession.setId(registerSession.getId());
        registerSessionRepository.save(registerSession);

        messageResponseType.setMessage(validation);
        return messageResponseType;
    }


    @Async
    public boolean isExistingAccount(String input) {

        boolean check = true;
        String name = "";
        String password = "";

        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/clients/" + input + "?detailsLevel=FULL");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/vnd.mambu.v2+json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();
            if (jsonString != null && !jsonString.isEmpty()) {
                check = true;
            }
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.getString("lastName").isEmpty()) {
                check = false;
            }
            String idno = "";
            JSONArray arr2 = jsonObject.getJSONArray("idDocuments");
            for (int i = 0; i < arr2.length(); ++i) {

                JSONObject jsn = arr2.getJSONObject(i);

                idno = jsn.getString("documentId");

            }

            registerSession.setId(registerSession.getId());
            registerSession.setNationalId(idno);
            registerSession.setFirstname(jsonObject.getString("firstName"));
            registerSession.setSurname(jsonObject.getString("lastName"));
            registerSession.setEcnumber(input);
            registerSessionRepository.save(registerSession);
            check=true;
        } catch (Exception f) {

            check = false;
        }


        return check;
    }
    @Async
    public boolean isExistingAccountMambu(String input) {

        boolean check = true;
        String name = "";
        String password = "";

        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/clients/" + input + "?detailsLevel=FULL");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/vnd.mambu.v2+json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();
            if (jsonString != null && !jsonString.isEmpty()) {
                check = true;
            }
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.getString("lastName").isEmpty()) {
                check = false;
            }
            String idno = "";
            JSONArray arr2 = jsonObject.getJSONArray("idDocuments");
            for (int i = 0; i < arr2.length(); ++i) {

                JSONObject jsn = arr2.getJSONObject(i);

                idno = jsn.getString("documentId");

            }

            registerSession.setId(registerSession.getId());
            registerSession.setNationalId(idno);
            registerSession.setFirstname(jsonObject.getString("firstName"));
            registerSession.setSurname(jsonObject.getString("lastName"));
            registerSessionRepository.save(registerSession);
            check=true;
        } catch (Exception f) {

            check = false;
        }


        return check;
    }
    @Async
    public String isExistingAccountMambuLoan(String input) {

        String check = "";
        String name = "";
        String password = "";

        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/loans/" + input);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();

            JSONObject jsonObject = new JSONObject(jsonString);
            check=jsonObject.getString("accountHolderKey");


        } catch (Exception f) {


        }


        return check;
    }

    @Override
    public boolean isRegisteredAccount(String input) {
        boolean check = false;
        String ecnumber = "";
        try {
            ecnumber = ussdMobileRepository.findByCDSNumber(input).getCDSNumber();

        } catch (Exception f) {
            ecnumber = "";
        }

        if (ecnumber.toLowerCase().trim().equalsIgnoreCase(input.toLowerCase().trim())) {
            check = true;
        }
        return check;
    }
}
