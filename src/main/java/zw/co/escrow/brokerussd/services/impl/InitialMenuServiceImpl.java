package zw.co.escrow.brokerussd.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zw.co.escrow.brokerussd.constants.NumbersEnum;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.subscriber.RegisterSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscribersRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.UssdMobileRepository;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.*;
import zw.co.escrow.brokerussd.services.GenericService;
import zw.co.escrow.brokerussd.services.InitialMenuService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Udean Mbano
 */

@Service
public class InitialMenuServiceImpl implements InitialMenuService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(InitialMenuServiceImpl.class);


    private SubscriberSessionRepository subscriberSessionRepository;
    private GenericService genericService;
    private RegisterSessionRepository registerSessionRepository;
    private SubscribersRepository subscribersRepository;
    private UssdMobileRepository ussdMobileRepository;

    @Autowired
    public InitialMenuServiceImpl(SubscriberSessionRepository subscriberSessionRepository,
                                  GenericService genericService,
                                  RegisterSessionRepository registerSessionRepository,
                                  SubscribersRepository subscribersRepository,
                                  UssdMobileRepository ussdMobileRepository) {
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.genericService = genericService;
        this.registerSessionRepository = registerSessionRepository;
        this.subscribersRepository = subscribersRepository;
        this.ussdMobileRepository = ussdMobileRepository;
    }

    @Override
    public MessageResponseType getInitialMenu(MessageRequestType request) {
        Random rand = new Random();
        Integer applicationId = rand.nextInt(1000) + 1;


        final String menuRegistered = "Welcome to Untu Capital Limited \n Select: \n" +
                "1.Apply Loan\n" +
                "2.Account Balance\n" +
                "3.Loans Status\n" +
                "4.Emergency Loan\n" +
                "5.Exit\n";
        final String menuUnRegistered = "Welcome to Untu Capital Limited \n Select: \n" +
                "1.Register\n" +
                "2.Exit\n";

        SubscriberSession isExist = subscriberSessionRepository.
                findByMobile(request.getSourceNumber());
        SubscriberSession subscriberSession = new SubscriberSession();

        if (isExist != null) {
            subscriberSession.setId(isExist.getId());
        }
        subscriberSession.setMobile(request.getSourceNumber());
        subscriberSession.setSessionComplete(false);
        subscriberSession.setRootMenu(StringsEnum.initial.toString());
        subscriberSession.setOnCancelOrders(false);
        subscriberSession.setCurrentPage(null);
        subscriberSession.setAmount(null);

        List<Subscribers> isRegistered = new ArrayList<>();

        isRegistered.clear();

        String strippedNumber = request.getSourceNumber().substring(3);

        isRegistered = subscribersRepository.findByPhoneNumber("%" + strippedNumber + "%");

        MessageResponseType response = new MessageResponseType();
        response.setTransactionID(request.getTransactionID());
        response.setDestinationNumber(request.getSourceNumber());
        response.setSourceNumber(request.getSourceNumber());
        response.setTransactionTime(request.getTransactionTime());
        response.setTransactionType(StringsEnum.menuProcessing.toString());
        response.setStage(StringsEnum.reqPending.toString());
        response.setChannel(request.getChannel());
        response.setApplicationTransactionID(StringsEnum.ctrade.toString().
                concat(applicationId.toString()));


        if (isRegistered == null || isRegistered.isEmpty()) {

            RegisterSession registerSession = genericService.getRegisterSession(request);

            if (registerSession == null) {
                registerSession = new RegisterSession();
            } else {
                registerSession.setId(registerSession.getId());
            }

            registerSession.setPhone(request.getSourceNumber());

            registerSession.setCurrentPage(0);

            if (registerSession.getOnBanks() == null)
                registerSession.setOnBanks(false);

            if (registerSession.getNameDetails() == null)
                registerSession.setNameDetails(false);

            if (registerSession.getBankDetails() == null)
                registerSession.setBankDetails(false);

            if (registerSession.getAddresses() == null)
                registerSession.setAddresses(false);

            if (registerSession.getAccount() == null)
                registerSession.setAccount(false);

            if (registerSession.getCustodianMenu() == null)
                registerSession.setCustodianMenu(false);

            if (registerSession.getPersonalDetails() == null)
                registerSession.setPersonalDetails(false);

            registerSessionRepository.save(registerSession);

            response.setStage(StringsEnum.reqPending.toString());
            response.setMessage(menuUnRegistered);
            subscriberSession.setMenu(StringsEnum.unRegOne.toString());
            subscriberSession.setRootMenu(StringsEnum.unRegOne.toString());
            subscriberSession.setRegistered(false);

        } else {

            subscriberSession.setRegistered(true);

            UssdMobile hasPin = ussdMobileRepository.findbyMobile("%" + strippedNumber + "%");

            String stage = request.getStage();

            if (hasPin == null) {

                if (stage.equalsIgnoreCase(StringsEnum.reqFirst.toString())) {

                    LOGGER.info("Setting new login pin {}", request.getSourceNumber());

                    response.setStage(StringsEnum.reqPending.toString());
                    response.setMessage("Please Set Up Your Login Pin:");
                    subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                    subscriberSession.setMenu(StringsEnum.regNewPin.toString());
                    UssdMobile ussdMobile = new UssdMobile();
                    ussdMobile.setMobile(request.getSourceNumber());
                    ussdMobile.setSessionComplete(false);
                    ussdMobile.setTransactionDate(genericService.getCurrentDate());
                    subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());
                    ussdMobileRepository.save(ussdMobile);

                }

            } else {


                long seconds = (genericService.getCurrentDate().getTime()
                        - hasPin.getTransactionDate().getTime()) / 1000;

                hasPin.setId(hasPin.getId());

                if (stage.equalsIgnoreCase(StringsEnum.reqFirst.toString())
                        && hasPin.isSessionComplete()) {

                    response.setStage(StringsEnum.reqPending.toString());
                    response.setMessage("Welcome to Untu Capital Limited \n" +" Please Enter Your Pin Or Press 1 to reset your Pin:");
                    subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                    subscriberSession.setMenu(StringsEnum.regPin.toString());
                    subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());

                } else if (stage.equalsIgnoreCase(StringsEnum.reqFirst.toString())
                        && !hasPin.isSessionComplete()
                        && seconds > 120) {

                    LOGGER.info("Session expired! {}", request.getSourceNumber());

                    response.setStage(StringsEnum.reqPending.toString());
                    response.setMessage("Please Set Up Your Login Pin:");
                    subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                    subscriberSession.setMenu(StringsEnum.regNewPin.toString());
                    hasPin.setSessionComplete(false);
                    hasPin.setTransactionDate(genericService.getCurrentDate());
                    subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());
                    ussdMobileRepository.save(hasPin);


                } else if (isExist != null &&
                        isExist.getMenu().equalsIgnoreCase(StringsEnum.regPin.toString()) |
                                isExist.getMenu().equalsIgnoreCase(StringsEnum.repayments.toString())) {

                    if (hasPin.getPin().equalsIgnoreCase(request.getMessage()) |
                            isExist.getMenu().equalsIgnoreCase(StringsEnum.repayments.toString())) {

                        response.setStage(StringsEnum.reqPending.toString());
                        response.setMessage(menuRegistered);
                        subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                        subscriberSession.setMenu(StringsEnum.regMenuOne.toString());
                        subscriberSession.setRootMenu(StringsEnum.regMenuOne.toString());
                        subscriberSession.setRegistered(true);

                    } else if (request.getMessage().equalsIgnoreCase(NumbersEnum.one.toString()) &
                            !isExist.getMenu().equalsIgnoreCase(StringsEnum.repayments.toString())) {
                        LOGGER.info("Reset pin {}", request.getSourceNumber());
                        response.setStage(StringsEnum.reqPending.toString());
                        response.setMessage("Please Provide Your ID-Number/Passport:");
                        subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                        subscriberSession.setMenu(StringsEnum.resetPinId.toString());
                        subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());
                        ussdMobileRepository.save(hasPin);

                    } else {

                        LOGGER.info("Wrong pin in logging in {}", request.getSourceNumber());

                        response.setStage(StringsEnum.reqComplete.toString());
                        response.setMessage(StringsEnum.passwordsNotMatch.toString());
                        subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                        subscriberSession.setMenu(StringsEnum.regConfirmPin.toString());
                        subscriberSession.setRootMenu("NONE");
                        subscriberSession.setRegistered(true);

                    }

                } else if (isExist != null &&
                        isExist.getMenu().equalsIgnoreCase(StringsEnum.resetPinId.toString())) {

                    String result =
                            genericService.isIdExists(request, request.getMessage(), request.getSourceNumber());

                    LOGGER.info("Id search response {}", result);

                    if (result.equalsIgnoreCase(NumbersEnum.one.toString())) {

                        response.setStage(StringsEnum.reqPending.toString());
                        response.setMessage("Please Set Up Your New Login Pin:");
                       hasPin.setSessionComplete(false);
                        hasPin.setTransactionDate(genericService.getCurrentDate());
                        ussdMobileRepository.save(hasPin);

                    } else {
                        response.setMessage("Id for this number does not exist. Please try again");
                        response.setStage(StringsEnum.reqComplete.toString());
                    }

                    subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                    subscriberSession.setMenu(StringsEnum.regNewPin.toString());
                    subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());

                } else if (isExist != null &&
                        isExist.getMenu().equalsIgnoreCase(StringsEnum.regNewPin.toString())) {

                    LOGGER.info("Confirm new login pin {}", request.getSourceNumber());

                    response.setStage(StringsEnum.reqPending.toString());
                    response.setMessage("Please Confirm Your New Login Pin:");
                    subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                    subscriberSession.setMenu(StringsEnum.regConfirmNewPin.toString());
                    hasPin.setPin(request.getMessage());
                    subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());
                    ussdMobileRepository.save(hasPin);

                } else if (isExist != null &&
                        isExist.getMenu().equalsIgnoreCase(StringsEnum.regConfirmNewPin.toString())) {

                    if (request.getMessage().equalsIgnoreCase(hasPin.getPin())) {

                        response.setStage(StringsEnum.reqComplete.toString());
                        response.setMessage("Your new login pin has been set. Please login now.");
                        subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                        subscriberSession.setMenu(StringsEnum.regConfirmPin.toString());
                        hasPin.setSessionComplete(true);
                        ussdMobileRepository.save(hasPin);
                        subscriberSession.setRootMenu("NONE");
                        subscriberSession.setRegistered(true);

                        LOGGER.info("New login pin set for {}", request.getSourceNumber());

                    } else {

                        LOGGER.info("Mismatch setting new pin {}", request.getSourceNumber());

                        response.setStage(StringsEnum.reqComplete.toString());
                        response.setMessage(StringsEnum.passwordsNotMatch.toString());
                        subscriberSession.setCdsNumber(isRegistered.get(0).getCdsNumber());
                        subscriberSession.setMenu(StringsEnum.regNewPin.toString());
                        subscriberSession.setRootMenu(StringsEnum.pinMenu.toString());
                        ussdMobileRepository.delete(hasPin);
                        subscriberSession.setRegistered(true);

                    }
                }

            }

        }

        subscriberSession.setTransactionDate(genericService.getCurrentDate());
        subscriberSessionRepository.save(subscriberSession);
        return response;
    }

}
