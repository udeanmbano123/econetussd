package zw.co.escrow.brokerussd.services;

import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.MessageResponseType;

/**
 * @author Udean Mbano
 */
public interface InitialMenuService {

    MessageResponseType getInitialMenu(MessageRequestType request);

}
