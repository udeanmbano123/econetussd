package zw.co.escrow.brokerussd.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zw.co.escrow.brokerussd.constants.StringsEnum;
import zw.co.escrow.brokerussd.persistance.ApiCredsR.ApiCredentialsRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.RegisterSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.SubscriberSessionRepository;
import zw.co.escrow.brokerussd.persistance.subscriber.TradeSessionRepository;
import zw.co.escrow.brokerussd.pojos.*;
import zw.co.escrow.brokerussd.pojos.ApiCreds.ApiCredentials;
import zw.co.escrow.brokerussd.pojos.persistance.CompanySecurities;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.*;
import zw.co.escrow.brokerussd.pojos.register.*;
import zw.co.escrow.brokerussd.services.GenericService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * @author Udean Mbano
 */
@Service
public class GenericServiceImpl implements GenericService {

    Logger LOGGER = LoggerFactory.getLogger(GenericServiceImpl.class);

    private SubscriberSessionRepository subscriberSessionRepository;
    private ApiCredentialsRepository apiCredentialsRepository;
    private TradeSessionRepository tradeSessionRepository;
    private RegisterSessionRepository registerSessionRepository;

    @Autowired
    public GenericServiceImpl(SubscriberSessionRepository subscriberSessionRepository,
                              TradeSessionRepository tradeSessionRepository,
                              RegisterSessionRepository registerSessionRepository, ApiCredentialsRepository apiCredentialsRepository) {
        this.subscriberSessionRepository = subscriberSessionRepository;
        this.tradeSessionRepository = tradeSessionRepository;
        this.registerSessionRepository = registerSessionRepository;
        this.apiCredentialsRepository = apiCredentialsRepository;
    }

    @Override
    public MessageResponseType exitUssd(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response);
        response.setMessage("Thank you for using UNTU application");
        return response;
    }
    @Override
    public MessageResponseType exitUssd(MessageRequestType request,String transactiontype) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response,transactiontype);

        response.setMessage("Thank you for using UNTU application");
        return response;
    }
    @Override
    public MessageResponseType invalidInput(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response);
        response.setMessage(StringsEnum.invalidInput.toString());
        LOGGER.info("Invalid input message: {}", request.getMessage());
        return response;
    }


    @Override
    public MessageResponseType getTerms(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response);
        response.setMessage(StringsEnum.terms.toString());
        return response;
    }

    @Override
    public MessageResponseType getReg(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response);
        response.setMessage(StringsEnum.terms.toString());
        return response;
    }

    @Override
    public MessageResponseType setFinalResponse(MessageRequestType request, MessageResponseType response) {
        response.setTransactionID(request.getTransactionID());
        response.setDestinationNumber(request.getSourceNumber());
        response.setSourceNumber(request.getSourceNumber());
        response.setTransactionTime(request.getTransactionTime());
        response.setStage(StringsEnum.reqComplete.toString());
        response.setTransactionType(StringsEnum.menuProcessing.toString());
        response.setChannel(request.getChannel());
        response.setApplicationTransactionID(getApplicationId());
        return response;
    }

    @Override
    public MessageResponseType setFinalResponse(MessageRequestType request, MessageResponseType response,String transactiontype) {
        response.setTransactionID(request.getTransactionID());
        response.setDestinationNumber(request.getSourceNumber());
        response.setSourceNumber(request.getSourceNumber());
        response.setTransactionTime(request.getTransactionTime());
        response.setStage(StringsEnum.reqComplete.toString());
        response.setTransactionType(transactiontype);
        response.setChannel(request.getChannel());
        response.setApplicationTransactionID(getApplicationId());
        return response;
    }
    @Override
    public Date getCurrentDate() {
        return new java.sql.Date(Calendar.getInstance().getTime().getTime());
    }

    @Override
    public SubscriberSession getSubscriberSession(MessageRequestType requestType) {
        return subscriberSessionRepository.findByMobile(requestType.getSourceNumber());
    }

    @Override
    public RegisterSession getRegisterSession(MessageRequestType request) {
        return registerSessionRepository.findByphone(request.getSourceNumber());
    }

    @Override
    public TradeSession getTradeSession(MessageRequestType request) {
        return tradeSessionRepository.findByMobile(request.getSourceNumber());
    }


    @Override
    public String isIdExists(MessageRequestType request, String id, String mobile) {
        String strippedNumber = request.getSourceNumber().substring(3);
        // final String uri = "http://192.168.3.245/EconetUssdApi/Finsec/getId?id=" + id + "&mobile=" + strippedNumber;
        //RestTemplate template = new RestTemplate();
        String check = "0";
        String ecnumber = "";
        try {
            ecnumber = registerSessionRepository.findByphone(request.getSourceNumber()).getNationalId();

        } catch (Exception f) {
            ecnumber = "";
        }

        if (ecnumber.toLowerCase().trim().equalsIgnoreCase(id.toLowerCase().trim())) {
            check = "1";
        }
        return check;
    }


    @Override
    public MessageResponseType setGenericResponse(MessageRequestType request,
                                                  MessageResponseType response) {
        response.setTransactionID(request.getTransactionID());
        response.setDestinationNumber(request.getSourceNumber());
        response.setSourceNumber(request.getSourceNumber());
        response.setTransactionTime(request.getTransactionTime());
        response.setTransactionType(StringsEnum.menuProcessing.toString());
        response.setStage(StringsEnum.reqPending.toString());
        response.setChannel(request.getChannel());
        response.setApplicationTransactionID(getApplicationId());
        return response;
    }

    @Override
    public MessageResponseType setGenericResponse(MessageRequestType request,
                                                  MessageResponseType response,String transactiontype) {
        response.setTransactionID(request.getTransactionID());
        response.setDestinationNumber(request.getSourceNumber());
        response.setSourceNumber(request.getSourceNumber());
        response.setTransactionTime(request.getTransactionTime());
        response.setTransactionType(transactiontype);
        response.setStage(StringsEnum.reqPending.toString());
        response.setChannel(request.getChannel());
        response.setApplicationTransactionID(getApplicationId());
        return response;
    }


    @Override
    public List<String> getMyTitles() {
        List<String> titles = new ArrayList<>();
        titles.add("Mr.");
        titles.add("Mrs.");
        titles.add("Miss.");
        titles.add("Ms.");
        return titles;
    }

    @Override
    public List<String> getGender() {
        List<String> gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");
        return gender;
    }

    @Override
    public List<String> getMyCountries() {
        List<String> countries = new ArrayList<>();
        countries.add("Zimbabwe");
        countries.add("South Africa");
        countries.add("Zambia");
        countries.add("Malawi");
        countries.add("Nigeria");
        countries.add("DRC");
        countries.add("Kenya");
        countries.add("Egypt");
        countries.add("India");
        return countries;
    }

    @Override
    public Map<String, String> allBanks() {
        Map<String, String> banks = new HashMap<>();
        banks.put("AGRIBANK", "AGRI");
        banks.put("BANC ABC", "BANC");
        banks.put("BARCLAYS", "BARZ");
        banks.put("CBZ", "CBZB");
        banks.put("CABS", "CABS");
        banks.put("CFX BANK", "CFX");
        banks.put("CBZ", "CBZ");
        banks.put("FBC", "FBC");
        banks.put("FIRST BANK BUILDING SOCIETY", "FBCB");
        banks.put("FIRST BANK", "FBCB");
        banks.put("INTERFIN", "INTERFIN");
        banks.put("KINGDOM", "KING");

        banks.put("MBCA", "MBCA");
        banks.put("FBC", "FBC");
        banks.put("METROPOLITAN  BANK", "METRO");
        banks.put("FIRST BANK", "FBCB");
        banks.put("INTERFIN", "NMB");
        banks.put("KINGDOM", "KING");
        return banks;
    }



    @Override
    public List<Bank> getBankList(MessageRequestType requestType) {
        final String uri = StringsEnum.apiUrl.toString() + "banks";
        RestTemplate template = new RestTemplate();
        List<Bank> banks = null;
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        try {
            banks = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, Bank.class));
        } catch (Exception e) {
            LOGGER.info("Error {}", e.toString());
        }
        return banks;
    }

    @Override
    public List<BankNext> getBanksNext(MessageRequestType requestType, String page) {
        final String uri = "http://192.168.3.245/EconetUssdApi/Finsec/getBanks?page=" + page;
        RestTemplate template = new RestTemplate();
        List<BankNext> banks = null;
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        try {
            banks = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, BankNext.class));
        } catch (Exception e) {
            LOGGER.info("Error {}", e.toString());
        }
        return banks;
    }



    @Override
    public List<Branch> getBranchList(MessageRequestType requestType) {
        RegisterSession registerSession = getRegisterSession(requestType);
        final String uri = StringsEnum.apiUrl.toString() +
                "GetBranchFromFullName?bank_name="
                + registerSession.getBank();
        RestTemplate template = new RestTemplate();
        List<Branch> branchList = null;
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        try {
            branchList = objectMapper.readValue(template.getForObject(uri, String.class),
                    typeFactory.constructCollectionType(List.class, Branch.class));
        } catch (Exception e) {
            LOGGER.info("Error {}", e.toString());
        }
        return branchList;
    }

    @Override
    public Boolean inputIsNumber(String input) {
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean isDateValid(String dateToValidate) {

        if (dateToValidate == null) {
            return false;
        }

        try {

            LocalDate localDate = LocalDate.parse(dateToValidate);

            LOGGER.info("Date passed {}", localDate);

        } catch (DateTimeParseException e) {

            LOGGER.info("Date parse exception {}", dateToValidate);
            return false;

        }
        return true;
    }

    @Override
    public Boolean inputIsDouble(String input) {
        try {
            double value = Double.parseDouble(input);
            double answer = value * 9 / 5 + 35;
            System.out.println(String.valueOf(answer));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public String getCdsNumber(MessageRequestType requestType) {
        SubscriberSession subscriber =
                subscriberSessionRepository.findByMobile(requestType.getSourceNumber());
        LOGGER.info("getCdsNumber {}", subscriber.getCdsNumber());
        return subscriber.getCdsNumber();
    }


    @Override
    public String getSSB(MessageRequestType requestType) {
        RegisterSession register=
                registerSessionRepository.findByphone(requestType.getSourceNumber());
        LOGGER.info("Get Custodian {}", register.getCustodian());
        return register.getCustodian();
    }


    @Override
    public String getApplicationId() {
        Random rand = new Random();
        Integer randNumber = rand.nextInt(1000) + 1;
        String appId;
        appId = StringsEnum.ctrade.toString()
                .concat(getCurrentDate().
                        toString()).concat(randNumber.toString());

        return appId;
    }

    @Override
    public Integer getSessionId() {
        Random r = new Random(System.currentTimeMillis());
        return 10000 + r.nextInt(20000);
    }

    @Override
    public String convertDate(Date date, String format) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(format);
        String sample = desiredFormat.format(date);

        return sample;
    }

    @Override
    public BigDecimal numberRounder(BigDecimal bigDecimal, Integer decimalPlaces) {
        BigDecimal roundOff = bigDecimal.setScale(decimalPlaces, BigDecimal.ROUND_HALF_EVEN);

        return roundOff;
    }

    @Override
    public boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    @Override
    public boolean isECNumber(String str) {
        boolean tru = false;
        int count = 0;
        for (int i = 0, len = str.length(); i < len; i++) {
            if (Character.isDigit(str.charAt(i))) {
                count++;
            }
        }
        System.out.println(count);
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) {
                counter++;
            }
        }

        if (count == 7 && counter == 1) {

            tru = true;
        }

        int n = 7;
        if (isNumeric(str.substring(0, n)) == false) {
            tru = false;
        }

        if (str.length() < 7) {
            tru = false;
        }

        return tru;
    }

    @Override
    public boolean isExistingAccount(String input) {

        String name = "";
        String password = "";


        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        System.out.println("auth string: " + authString);
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/clients/" + input + "?detailsLevel=FULL");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/vnd.mambu.v2+json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();

            JSONObject jsonObject = new JSONObject(jsonString);
            System.out.println("First Name" + jsonObject.getString("firstName") + "\n");
            System.out.println("Last Name" + jsonObject.getString("lastName") + "\n");
            String idno = "";
            JSONArray arr2 = jsonObject.getJSONArray("idDocuments");
            for (int i = 0; i < arr2.length(); ++i) {

                JSONObject jsn = arr2.getJSONObject(i);

                idno = jsn.getString("documentId");

            }
            System.out.println(idno + "\n");
        } catch (Exception f) {

        }


        return true;
    }

    @Override
    public boolean isRegisteredAccount(String input) {
        boolean check = false;
        String ecnumber = "";
        try {
            ecnumber = registerSessionRepository.findByEcnumber(input).getEcNumber();

        } catch (Exception f) {
            ecnumber = "";
        }

        if (ecnumber.toLowerCase().trim().equalsIgnoreCase(input.toLowerCase().trim())) {
            check = true;
        }
        System.out.println("The prompted ec number " + input + "\n");
        System.out.println("The ec number " + ecnumber + "\n");
        System.out.println("Do they match " + check + "\n");
        return check;
    }

    public boolean isNullOrEmpty(String str) {

        boolean result = false;

        if (str.trim().replaceAll("@","").length() < 1) result = true;


        return result;
    }

    @Async
    public void SendSms(String messages,String mobile) {

        //you have been successfully+19293963329
        try {
           /* final String ACCOUNT_SID = "ACb039356b48f7e1357e744ff67d7db32f";
            final String AUTH_TOKEN = "64644c420ef1e02ca43b8c1d90ca4c68";
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
            Message message = Message.creator(
                    new com.twilio.type.PhoneNumber(mobile),
                    new com.twilio.type.PhoneNumber("UntuCapital"),
                    messages)
                    .create();
            message.getSid();
                   */
        } catch (Exception f) {

        }

            }
    public String decode(String url)
    {
        try {
            String prevURL="";
            String decodeURL=url;
            while(!prevURL.equals(decodeURL))
            {
                prevURL=decodeURL;
                decodeURL=URLDecoder.decode( decodeURL, "UTF-8" );
            }
            return decodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while decoding" +e.getMessage();
        }
    }
    public  String encode(String url)
    {
        try {
            String encodeURL=URLEncoder.encode( url, "UTF-8" );
            return encodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while encoding" +e.getMessage();
        }
    }
    @Override
    public   MessageResponseType SmsNotification(MessageRequestType request) {
        MessageResponseType response = new MessageResponseType();
        setFinalResponse(request, response);
        response.setMessage("You will receive an Sms shortly");
       return response;
    }

    @Override
    public String isLoanBalance(String input) {
        boolean check = true;
        String name = "";
        String password = "";

        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        float totalBalance=0;
        float InterestBalance=0;
         float InterestAccrued=0;
         float   FeeBalance=0;
        float  PenaltyBalance=0;

        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/clients/" + input + "/loans?accountState=ACTIVE");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();

            try {
                JSONArray objs = new JSONArray(jsonString); // parse the array
                for(int i = 0; i < objs.length(); i++){ // iterate over the arrayprincipalBalance
                    JSONObject o = objs.getJSONObject(i);
                    String id = o.getString("id");
                    String principalBalance = o.getString("principalBalance");
                    String accountState=o.getString("accountState");
                    double pBalance = Double.parseDouble(principalBalance);
                    double pInterestBalance= Double.parseDouble(o.getString("interestBalance"));
                    double pInterestAccrued= Double.parseDouble(o.getString("accruedInterest"));
                    double pFeeBalance= Double.parseDouble(o.getString("feesBalance"));
                    double pPenaltyBalance= Double.parseDouble(o.getString("penaltyBalance"));
                    if(accountState.equalsIgnoreCase("ACTIVE_IN_ARREARS")){
                        totalBalance+=pBalance;
                        InterestBalance+=pInterestBalance;
                        InterestAccrued+=pInterestAccrued;
                       FeeBalance+=pFeeBalance;
                        PenaltyBalance+=pPenaltyBalance;
                    }
                    if(accountState.equalsIgnoreCase("ACTIVE")){
                        totalBalance+=pBalance;
                    }
                }
            } catch (JSONException e){
                System.out.println(e.getMessage());
            }

            System.out.println("===Total balance is===: $ZWL" + totalBalance);

        } catch (Exception f) {


        }
        /*totalBalance+=pBalance;
        InterestBalance+=pInterestBalance;
        InterestAccrued+=pInterestAccrued;
        FeeBalance+=pFeeBalance;
        PenaltyBalance+=pPenaltyBalance;*/
        String result="Principal Balance ZWL$"+totalBalance+"\n";
        result+="Interest Balance ZWL$"+InterestBalance+"\n";

        result+="Interest Accrued ZWL$"+InterestAccrued+"\n";

        result+="Fee Balance ZWL$"+FeeBalance+"\n";

        result+="Penalty Balance ZWL$"+PenaltyBalance;
        float fullBalance=totalBalance+InterestBalance+InterestAccrued+FeeBalance;
        result="Balance ZWL$"+fullBalance;
        return result;
    }
    @Override
    public String isLoanBalanceMambu(String input) {
        boolean check = true;
        String name = "";
        String password = "";

        StringBuffer buffer = new StringBuffer();

        List<ApiCredentials> apiCredentialsList = apiCredentialsRepository.findAll();

        int max = 1;

        if (apiCredentialsList.size() <= 1)
            max = apiCredentialsList.size();

        for (int itemNumber = 1, index = 0; index < max; index++, itemNumber++) {

            name = apiCredentialsList.get(index).getUsername();
            password = apiCredentialsList.get(index).getPassword();
        }
        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        float totalBalance=0;
        float InterestBalance=0;
        float InterestAccrued=0;
        float   FeeBalance=0;
        float  PenaltyBalance=0;

        try {


            URL obj = new URL("https://untucapital.sandbox.mambu.com/api/loans/" + input);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String jsonString = response.toString();

            try {
                JSONArray objs = new JSONArray(jsonString); // parse the array
                for(int i = 0; i < objs.length(); i++){ // iterate over the arrayprincipalBalance
                    JSONObject o = objs.getJSONObject(i);
                    String id = o.getString("id");
                    String principalBalance = o.getString("principalBalance");
                    String accountState=o.getString("accountState");
                    double pBalance = Double.parseDouble(principalBalance);
                    double pInterestBalance= Double.parseDouble(o.getString("interestBalance"));
                    double pInterestAccrued= Double.parseDouble(o.getString("accruedInterest"));
                    double pFeeBalance= Double.parseDouble(o.getString("feesBalance"));
                    double pPenaltyBalance= Double.parseDouble(o.getString("penaltyBalance"));
                    if(accountState.equalsIgnoreCase("ACTIVE_IN_ARREARS")){
                        totalBalance+=pBalance;
                        InterestBalance+=pInterestBalance;
                        InterestAccrued+=pInterestAccrued;
                        FeeBalance+=pFeeBalance;
                        PenaltyBalance+=pPenaltyBalance;
                    }
                    if(accountState.equalsIgnoreCase("ACTIVE")){
                        totalBalance+=pBalance;
                    }
                }
            } catch (JSONException e){
                System.out.println(e.getMessage());
            }

            System.out.println("===Total balance is===: $ZWL" + totalBalance);

        } catch (Exception f) {


        }
        /*totalBalance+=pBalance;
        InterestBalance+=pInterestBalance;
        InterestAccrued+=pInterestAccrued;
        FeeBalance+=pFeeBalance;
        PenaltyBalance+=pPenaltyBalance;*/
        String result="Principal Balance ZWL$"+totalBalance+"\n";
        result+="Interest Balance ZWL$"+InterestBalance+"\n";

        result+="Interest Accrued ZWL$"+InterestAccrued+"\n";

        result+="Fee Balance ZWL$"+FeeBalance+"\n";

        result+="Penalty Balance ZWL$"+PenaltyBalance;
        float fullBalance=totalBalance+InterestBalance+InterestAccrued+FeeBalance;
        result="Balance ZWL$"+fullBalance;
        return result;
    }
    @Override
    public boolean isAuthAccount(String input) {
        boolean check = false;
        boolean status = false;
        try {
            status = registerSessionRepository.findByphone(input).getAccount();

        } catch (Exception f) {

        }


        return status;
    }
}
