package zw.co.escrow.brokerussd.constants;

/**
 * @author Godwin Tavirimirwa
 */
public enum StringsEnum {

    apiUrl("https://demo.ctrade.co.zw/api/"),
    mobileApiUrl("https://demo.ctrade.co.zw/mobileapi/"),
    regUrl("https://demo.ctrade.co.zw/online/online.ctrade_php/func/createacc_mobile.php"),

    regMenuOne("R1"),
    regPin("PIN"),
    pinMenu("PINMENU"),
    regConfirmPin("CONFIRMPIN"),
    regConfirmNewPin("CONFIRMNEWPIN"),
    regNewPin("NPIN"),
    reqFirst("FIRST"),
    reqPending("PENDING"),
    reqComplete("COMPLETE"),
    regDepoConfirm("RDC"),
    withdrawAmount("WDAMT"),
    rootWithdrawal("ROOTWITH"),
    withdrawConfirm("WTHDCONFIRM"),
    regDepoFinal("RDF"),
    unRegOne("NR1"),
    menuProcessing("MENU_PROCESSING"),
    trade("ONTRADE"),
    myFundsOne("MF"),
    tradeMarket("TM"),
    tradeMarketTradeType("TMTT"),
    tradeOrderQuantity("TOQ"),
    tradeConfirmOrder("TCOF"),
    tradeOnCompanies("TGMC"),
    tradeBroker("TB"),
    defaultTradeSecurity("EQUITY"),
    ctrade("ctrade_"),
    zse("ZSE"),
    finsec("FINSEC"),
    initial("INIT"),
    tradePrice("TP"),
    deposit("ONDEPOSIT"),
    sell("SELL"),
    buy("BUY"),
    zseCompaniesMenu("ZSECOMPMENU"),
    priceprotection("You entered price outside price protection zone."),
    registrationCancelled("You terminated the registration process! Please resume again next time."),
    defaultTimeInForce("Day Order(DO)"),
    personalDetails("Personal details"),
    registerOthersFirst("Sorry, please register " +
            "execute prior menus first and then come to this one, thank you."),
    invalidInput("The option you supplied is invalid. Please try again."),
    terms("Please visit https://ctrade.co.zw/online/docs/terms.pdf for C-Trade Terms & Conditions."),
    ipoNotPresent("No IPO currently open."),
    bankDetails("Bank details"),
    addresses("Addresses"),
    nameDetails("Name details"),
    custodianDetails("Custodian details"),
    existsAlready("EXISTAL"),
    completePersonalDetails("CPERSONALDETAILS"),
    completeNameDetails("COMPNAMEDETAILS"),
    completeAddresses("COMPADS"),
    completeBankDetails("COMPBANKD"),
    inTheSystem("You are already registered. Please proceed to login."),
    alreadyRegistered(" are already registered. Please proceed to the next menu."),
    finishedRegistered(" registration complete! Please register "),
    regCompleteChoices("\nPlease Select:\n1.Next Menu\n2.Exit"),
    regTitle("REGTTL"),
    regSurname("REGSURN"),
    regTenure("REGTEN"),
    regTenure2("REGTEN2"),
    regEC("REGEC"),
    regMB("REGEC"),
    /*regMenu("Please Complete All menus sequentially to create an account.\n" +
            "1.Name Details\n" +
            "2.Personal Details\n" +
            "3.Addresses\n" +
            "4.Bank Details\n" +
            "5.Custodians\n" +
            "6.Complete Process\n"),*/
    regMenu("Registration\n" +
            "1.New Client\n" +
            "2.Existing Customer\n"),
    regMenuClient("New Client Registration\n" +
            "1.Ordinary Client\n" +
            "2.SSB CLIENT\n"),
    regMenuClientLink("New Client Registration\n" +
            "1.SSB Client\n" +
            "2.Ordinary Client\n"),
    loanMenu("My Loans\n" +
            "1.Apply Instant Loan \n" +
            "2.Apply General Loan \n"),
    accountMenu("My Account\n" +
            "1.Repayment\n" +
            "2.View Account Status\n" +
            "3.Account Balance\n"),
    regName("REGNAME"),
    regConfirmNameDetails("REGCONNAMED"),
    regConfirmLoanDetails("REGCONLOANDD"),
    regConfirmLoanDetailsFinal("REGCONLOANDFINAL"),
    regConfirmLoanAmount("REGCONLOANDQ"),
    regConfirmLoanCommit("REGCONLOANCOM"),
    regConfirmLoanDetailsCommit("REGCONLOANDCOMMIT"),
    regNatId("REGNATID"),
    regNationality("REGNATALIY"),
    regDob("REGDOB"),
    regSubMenu("REGSUBMENU"),
    loanSubMenu("LOSUBMENU"),
    clientSubMenu("NEWCLIENTSUBMENU"),
    AccSubMenu("ACCSUBMENU"),
    regPersonalDetails("REGPDETAILS"),
    regNameDetails("REGNAMEDETAILS"),
    appLoanDetails("LONNAMEDETAILS"),
    regAddresses("REGADDRESS"),
    regBankDetails("REGBANKDETAILS"),
    regAccount("REGACCOUNT"),
    regComplete("Account successfully created! Please dial *727# again to login to your account."),
    passwordsNotMatch("The password provided did not match. Please try again."),
    pinsNotMatch("The USSD pins provided did not match. Please try again."),
    regAddress("REGAD"),
    registrationError("Something went wrong in creating your account. Please contact support team."),
    regGender("REGGEN"),
    accountSuccess("Account successfully created"),
    emailExistsError("Email already exists"),
    emailAlreadyExists("Supplied email is already registered please contact support team."),
    idExistsError("Failure IDNo./RegNo.AlreadyRegistered"),
    idAlreadyExists("National Id supplied is already registered please contact support team."),
    missingFieldsError("Missing ID No./Reg No."),
    missingIdNoError("Missing ID No./Reg No."),
    invalidBrokerError("Invalid Broker"),
    errorCreatingAccount("Error Creating Account"),
    comingSoon("Coming soon !"),

    regCustodian("REGCUSTO"),
    regBank("REGBANK"),
    regBranch("REGBRANCH"),
    defaultBranch("HEAD"),
    regAccountNo("REGACCNO"),
    regEmail("REGEMAIL"),
    regPass("REGPASS"),
    regConfirmPass("REGCONPASS"),
    regecPass("REGECPASS"),
    regConfirmPass2("REGCONPASS2"),
    regMambuPass("REGMAMBUPASS"),
    regConfirmNew("REGCONNEW"),
    regConfirmlink("REGCONLINK"),
    confirmCompleteReg("REGCONREG"),
    confirmCompleteLon("REGCONLON"),
    regCustodianMenu("RCUSTOMENU"),
    regCompleteCustodian("RCOMPLETECUSTO"),
    completeProcess("Complete Process"),
    regConfirmMenuTwoDetails("REGCONM2DET"), bankDetailsComplete("Registration phase two successfully completed. " +
            "Please select:\n1.Complete process. 2.Exit"),
    nameDetailsComplete("You have successfully created an account with Untu Capital Ltd. Dial *261# for more interesting offers!."),
    loanDetailsComplete("Loan successfully submitted."),
    regGetToCancelOrders("RGOTC"),
    regCancelOrders("RCOS"),
    regConfirmCancelOrders("RCCORDS"),
    regCompleteCancelOrders("REGCOMCANORD"), buysOrSells("BOS"),
    selectedCounterDetails("SCDT"), stocksOrCounters("SOCTRS"),
    resetPinId("RPID"),
    initmenu1("REG1"),
    initmenu2("MYLON"),
    initmenu3("MYACC"),
    contmenu("1.Main Menu\n" +
             "2.Exit"),
    contmenu2("1.Main Menu\n" +
            "2.Exit"),
    resetPinIdConfirm("RIDCONF"),
    regConfirmExit("CONFIRMEXIT"),
    repayments("REPAYM");
    private final String text;

    StringsEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
