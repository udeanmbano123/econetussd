package zw.co.escrow.brokerussd.constants;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class MultiDataTypesFormatter {

    public String toTwoDecimalPlaces(String value) {
        float original = Float.valueOf(value);

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,##0.00");
        String formattedValue = decimalFormat.format(original);

        return String.valueOf(formattedValue);
    }

    public String thousandSaperator(String value) {
        float original = Float.valueOf(value);

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###,###");
        String formattedValue = decimalFormat.format(original);

        return String.valueOf(formattedValue);
    }

    public static boolean noSpecialChars(String input) {

        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/<>.^*()%!-]");

        return !regex.matcher(input).find();

    }

    public String toLocateDate(String date) {

        try {

            if (date.charAt(4) == ' ') {
                StringBuilder formattedDate = new StringBuilder(date);
                formattedDate.setCharAt(4, '0');

                DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM dd yyyy");
                LocalDate dt = formatter.parseLocalDate(formattedDate.toString());
                return dt.toString();
            } else {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM dd yyyy");
                LocalDate dt = formatter.parseLocalDate(date);
                return dt.toString();
            }

        } catch (Exception e) {
            System.out.println("Error is passing date is " + e.toString());
            return null;
        }
    }

    public String toStandardLocalDate(String date) {

        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("d MMM yyyy");

            LocalDate dt = formatter.parseLocalDate(date);
            return dt.toString();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    public String capitalizeFirstLetter(String word) {
        String rawWordtoLowerCase = word.toLowerCase();
        String desiredWord = rawWordtoLowerCase.substring(0, 1)
                .toUpperCase() + rawWordtoLowerCase.substring(1);
        return desiredWord;
    }

}
