package zw.co.escrow.brokerussd.constants;

/**
 * @author Godwin Tavirimirwa
 */
public enum NumbersEnum {

    zero("0"),
    one("1"),
    two("2"),
    three("3"),
    four("4"),
    five("5"),
    six("6"),
    seven("7"),
    eight("8"),
    nine("9");


    private final String text;

    NumbersEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
