package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.RegisterSession;

/**
 * @author Godwin Tavirimirwa
 */
public interface RegisterSessionRepository extends JpaRepository<RegisterSession, Integer> {

    RegisterSession findByphone(String mobile);
    RegisterSession findByEcnumber(String ecnumber);
    RegisterSession findByCustodian(String mobile);

}
