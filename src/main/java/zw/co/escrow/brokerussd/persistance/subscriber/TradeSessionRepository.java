package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.MessageRequestType;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.TradeSession;

/**
 * @author Godwin Tavirimirwa
 */
public interface TradeSessionRepository extends JpaRepository<TradeSession, Integer> {

    TradeSession findByMobile(String mobile);
}
