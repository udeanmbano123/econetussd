package zw.co.escrow.brokerussd.persistance.data;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.persistance.data.Data;
/**
 * @author Godwin Tavirimirwa
 */
public interface DataRepository extends JpaRepository<Data, Integer> {
}
