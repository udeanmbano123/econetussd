package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.loans.Loans;

import java.util.List;

/**
 * @author Udean Mbano
 */
public interface LoansRepository  extends JpaRepository<Loans, Integer> {

    List<Loans> findByMobile(String mobile);
}
