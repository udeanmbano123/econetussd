package zw.co.escrow.brokerussd.persistance.ApiCredsR;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.ApiCreds.ApiCredentials;

/**
 * @author Godwin Tavirimirwa
 */
public interface ApiCredentialsRepository   extends JpaRepository<ApiCredentials, Integer> {

}
