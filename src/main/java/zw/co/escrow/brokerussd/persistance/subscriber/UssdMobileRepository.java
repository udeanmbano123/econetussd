package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.UssdMobile;

/**
 * @author Godwin Tavirimirwa
 */
public interface UssdMobileRepository extends JpaRepository<UssdMobile, Integer> {

    @Query(value = "select top 1 * from ussd_mobile where mobile like :mobile",
            nativeQuery = true)
    UssdMobile findbyMobile(@Param("mobile") String mobile);
    UssdMobile findByCDSNumber(@Param("CDS_Number") String CDS_Number);

}
