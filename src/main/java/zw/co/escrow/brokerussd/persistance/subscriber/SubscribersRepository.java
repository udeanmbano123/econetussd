package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.Subscribers;

import java.util.List;

/**
 * @author Godwin Tavirimirwa
 */
public interface SubscribersRepository extends JpaRepository<Subscribers, Integer> {

    //@Query("SELECT s FROM Subscribers s where s.phoneNumber like :phone")
    @Query(value = "SELECT  [Id],replace([PhoneNumber],' ','') as PhoneNumber ,[Email] ,[Username] ,[CdsNumber]," +
            "[Password] ,[Active]  ,[Date] ,[PIN],[EmailSend] FROM [Subscribers] where replace([PhoneNumber],' ','') " +
            " like replace(:phone,' ','')",
            nativeQuery = true)
    List<Subscribers> findByPhoneNumber(@Param("phone") String phone);
}
