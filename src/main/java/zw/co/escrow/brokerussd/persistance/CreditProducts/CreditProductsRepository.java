package zw.co.escrow.brokerussd.persistance.CreditProducts;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.ApiCreds.ApiCredentials;
import zw.co.escrow.brokerussd.pojos.CreditProducts.CreditProducts;

import java.util.List;

/**
 * @author Godwin Tavirimirwa
 */
public interface CreditProductsRepository extends JpaRepository<CreditProducts, Integer> {

    List<CreditProducts> findByDisplayName(String loan);
}
