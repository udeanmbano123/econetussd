package zw.co.escrow.brokerussd.persistance.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.escrow.brokerussd.pojos.persistance.subscriber.SubscriberSession;
/**
 * @author Godwin Tavirimirwa
 */
public interface SubscriberSessionRepository extends JpaRepository<SubscriberSession, Long> {

    SubscriberSession findByMobile(String mobile);
}
